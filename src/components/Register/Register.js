/**
 * Created by i.chernyakov on 18.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';

import NewAccount from '../common/NewAccount/NewAccount';

class Register extends Component {

    render() {
        const breadCrumbs = [{title: 'Регистрация'}];
        return(
            <div>
                <NewAccount
                    breadCrumbs={breadCrumbs}
                />
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);

Register.propTypes  = {
    //listData: PropTypes.array.isRequired
};
