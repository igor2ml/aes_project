/**
 * Created by i.chernyakov on 18.11.2017.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


import MainMenu from '../MainMenu/MainMenu';
import Authorization from '../Authorization/Authorization';

class StartPage extends Component {

    render() {
        const isLogin = this.props.user.isLogin;
        // const isLogin = false;

        return(
            <div>
                {isLogin &&
                    <MainMenu/>
                }
                {!isLogin &&
                    <Authorization/>
                }

            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        user: state.settings.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartPage);


import PropTypes from 'prop-types';
StartPage.propTypes  = {
    //listData: PropTypes.array.isRequired
};
