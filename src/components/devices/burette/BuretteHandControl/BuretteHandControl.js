/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, RaisedButton} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import {setBuretteEmpty, setBuretteFill, setBuretteRinse} from '../../../../actions/buretteActions';

class BuretteHandControl extends Component {

    rinseBurette(){
        const buretteNumber = this.props.match.params.id;
        this.props.pageActions.setBuretteRinse(buretteNumber);
    }

    fillBurette(){
        const buretteNumber = this.props.match.params.id;
        this.props.pageActions.setBuretteFill(buretteNumber);
    }

    emptyBurette() {
        const buretteNumber = this.props.match.params.id;
        this.props.pageActions.setBuretteEmpty(buretteNumber);
    }

    render() {


        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Бюретка', link: '/device--burette'}, {title: 'Ручное управление'}];
        const style = {
            subheader: {
                padding: 0,
                lineHeight: '20px',
                margin: '20px 0 20px 0'
                // maxWidth: 400
            }
        };
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <Row>
                                <Col xs4={4} xs8={4} sm={6} md={4} mdOffset={2} lg={4} lgOffset={2}>
                                    <p>Автоматическая промывка бюретки и жидкостного тракта, удаление пузырьков</p>
                                    <RaisedButton
                                        label={'Промывка'}
                                        primary={true}
                                        onClick={this.rinseBurette.bind(this)}
                                        style={{width: 200, height: 100}}
                                        labelStyle={{fontSize: 20}}
                                    />
                                    <br/><br/>
                                </Col>
                                <Col xs4={4} xs8={4} sm={6} md={4} mdOffset={0} lg={4} lgOffset={0}>
                                    <br/><br/><br/>
                                    <RaisedButton
                                        //label={<span><i className='fa fa-arrow-up' aria-hidden='true'></i> Заполнить</span>}
                                        label="Заполнить"
                                        primary={true}
                                        onClick={this.fillBurette.bind(this)}
                                    />
                                    <p>Заполнение бюретки</p>
                                    <RaisedButton
                                        //label={<span><i className='fa fa-arrow-down' aria-hidden='true'></i> Опорожнить </span>}
                                        label="Опорожнить"
                                        primary={true}
                                        onClick={this.emptyBurette.bind(this)}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            setBuretteEmpty,
            setBuretteFill,
            setBuretteRinse
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BuretteHandControl);

import PropTypes from 'prop-types';
BuretteHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
};


