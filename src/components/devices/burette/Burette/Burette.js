/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton, FlatButton, SelectField, MenuItem} from 'material-ui';
import {Tabs, Tab} from 'material-ui/Tabs';
import {CardHeader} from 'material-ui/Card';
import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';


class Burette extends Component {

    constructor(props){
        super(props);
        this.state = {
            value: '2'
        }
    }

    handleChange = (value) => {
        this.setState({
            value: value,
        });
    };

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Бюретка', link: '/device--burette'}];
        const styles = {
            headline: {
                fontSize: 24,
                paddingTop: 16,
                marginBottom: 12,
                fontWeight: 400,
            },
        };
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                            <br/>
                            <Tabs
                                value={this.state.value}
                                onChange={this.handleChange.bind(this)}
                            >
                                <Tab label="Бюретка 1" value="2">
                                    <Content burette={{name: 1, number: 0}}/>
                                </Tab>
                                <Tab label="Бюретка 2" value="3">
                                    <Content burette={{name: 2, number: 1}}/>
                                </Tab>
                                <Tab label="Бюретка 3" value="4">
                                    <Content burette={{name: 3, number: 2}}/>
                                </Tab>
                            </Tabs>

                        </Col>
                    </Row>

                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Burette);

import PropTypes from 'prop-types';
Burette.propTypes  = {
    //listData: PropTypes.array.isRequired
};


class Content extends Component {
    constructor(props){
        super(props);
        this.state = {
            device: 'Бюретка ' + this.props.burette.name,
            name: 'Бюретка 1 CHL',
            sn: '123456',
            volume: '30',
            titrant: 'HCL',
            titr_theor: '0,1',
            kp: '1'
        }
    }
    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    onSave(){
        toastr.success('Сохранение', 'Параметры сохранены');
    }
    render() {

        const burreteNumber = this.props.burette.number;

        return <div>
            <br/>
            <Row
                middle={['xs4', 'sm', 'md', 'ld']}
            >
                <Col xs4={4} xs8={8} sm={12} md={10} mdOffset={1} lg={10} lgOffset={1}>
                    <Row>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <CardHeader
                                title="Устройство"
                                subtitle={this.state.device}
                            />
                        </Col>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <KeyBoard
                                value={this.state.titrant}
                                hintText="Титрант"
                                floatingLabelText="Титрант"
                                onChange={(value)=>{this.onChangeInput('titrant', value)}}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs4={4} xs8={4} sm={4} md={4} lg={4}>
                            <CardHeader
                                title="Обьем, мл"
                                subtitle={this.state.volume}
                            />
                        </Col>
                        <Col xs4={4} xs8={4} sm={4} md={4} lg={4}>
                            <KeyBoard
                                value={this.state.titr_theor}
                                hintText="Титр теор."
                                floatingLabelText="Титр теор."
                                onChange={(value)=>{this.onChangeInput('titr_theor', value)}}
                            />
                        </Col>
                        <Col xs4={4} xs8={4} sm={4} md={4} lg={4}>
                            <SelectField floatingLabelText="Единицы измерения" value={1}>
                                <MenuItem value={1} primaryText="моль/дм3" />
                                <MenuItem value={2} primaryText="ммоль/дм3 " />
                                <MenuItem value={3} primaryText="мг/дм3 " />
                                <MenuItem value={4} primaryText="г/дм3 " />
                                <MenuItem value={5} primaryText="г/кг" />
                                <MenuItem value={6} primaryText="ppm" />
                                <MenuItem value={7} primaryText="%" />
                                <MenuItem value={8} primaryText="об.%" />
                            </SelectField><br/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <CardHeader
                                title="Калибровка"
                                subtitle={'12.05.2017'}
                            />
                        </Col>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <KeyBoard
                                value={this.state.kp}
                                hintText="Кп"
                                floatingLabelText="Кп"
                                onChange={(value)=>{this.onChangeInput('kp', value)}}
                            />
                        </Col>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <CardHeader
                                title="Дата определения Кп"
                                subtitle={'12.05.2017'}
                            />
                        </Col>
                    </Row>
                    <br/><br/>
                    <Row center={['sm', 'md', 'lg']}>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <Link to={'/device--burette--calibration-' + burreteNumber }>
                                <RaisedButton
                                    label="Калибровка"
                                />
                            </Link>
                        </Col>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <Link to={'/device--burette--hand-control-' + burreteNumber}>
                                <RaisedButton
                                    label="Ручное управление"
                                />
                            </Link>
                        </Col>
                        <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                            <Link to={'/device--burette--kp-' + burreteNumber}>
                                <RaisedButton
                                    label="Определение Кп"
                                />
                            </Link>
                        </Col>
                    </Row>
                    <br/><br/>
                    <Row center={['sm', 'md', 'lg']}>
                        <Col xs4={4} xs8={8} sm={6} md={6} lg={6}>
                            <RaisedButton
                                label="Отменить"
                                primary={true}
                            />
                        </Col>
                        <Col xs4={4} xs8={8} sm={6} md={6} lg={6}>
                            <RaisedButton
                                label="Сохранить"
                                primary={true}
                                onClick={this.onSave.bind(this)}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    }
}