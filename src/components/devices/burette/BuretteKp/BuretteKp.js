/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, RaisedButton} from 'material-ui';
import {CardHeader} from 'material-ui/Card';

import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';

import './BuretteKp.sass';

class BuretteKp extends Component {

    constructor(props){
        super(props);
        this.state = {
            v_theory: '10,0',
            count_points: '1',
            sensitivity: '10'
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    
    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Бюретка', link: '/device--burette'}, {title: 'Определение Кп'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={10} lg={10} mdOffset={1} lgOffset={1}>
                            <Row>
                                <Col sm={6} md={6} lg={6}>
                                    <SelectField
                                        floatingLabelText={'Позиция'}
                                        value={1}
                                    >
                                        <MenuItem value={1} primaryText="1" />
                                        <MenuItem value={2} primaryText="2" />
                                    </SelectField>
                                </Col>
                                <Col sm={6} md={6} lg={6}>
                                    <KeyBoard
                                        value={this.state.count_points}
                                        floatingLabelText="Количество точек"
                                        onChange={(value)=>{this.onChangeInput('count_points', value)}}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6} md={6} lg={6}>
                                    <KeyBoard
                                        value={this.state.v_theory}
                                        floatingLabelText="V теор., мл "
                                        onChange={(value)=>{this.onChangeInput('v_theory', value)}}
                                    />
                                </Col>
                                <Col sm={6} md={6} lg={6}>
                                    <KeyBoard
                                        value={this.state.sensitivity}
                                        floatingLabelText="Чувствительность"
                                        onChange={(value)=>{this.onChangeInput('sensitivity', value)}}
                                    />
                                </Col>
                            </Row>
                            <br/>
                            <Row center={['sm', 'md', 'lg']}>
                                <Col sm={6} md={6} lg={6}>
                                    <CardHeader
                                        title={'Статус:'}
                                        subtitle={'Выполняется определение КП'}
                                        //или
                                        //subtitle={'Выполнено. Новый КП=0,98 '}
                                    />
                                </Col>
                            </Row>
                            <br/><br/>
                            <Row center={['sm', 'md', 'lg']}>
                                <Col sm={4} md={4} lg={4}>
                                    <RaisedButton
                                        label={'Отмена'}
                                        primary={true}
                                        onClick={()=>{this.props.history.goBack()}}
                                    />
                                </Col>
                                <Col sm={4} md={4} lg={4}>
                                    <RaisedButton
                                        label={'Определить Кп'}
                                        primary={true}
                                    />
                                </Col>
                                <Col sm={4} md={4} lg={4}>
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                    />
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BuretteKp));

import PropTypes from 'prop-types';
BuretteKp.propTypes  = {
    //listData: PropTypes.array.isRequired
};

