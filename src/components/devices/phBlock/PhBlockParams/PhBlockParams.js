/**
 * Created by i.chernyakov on 19.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {TextField, RaisedButton, FlatButton, SelectField, MenuItem} from 'material-ui';

import {TopBar, KeyBoard} from 'blocks';
import {style} from 'config';

import './PhBlockParams.sass';

class PhBlockParams extends Component {

    constructor(props){
        super(props);
        this.state = {
            pxi: '6,86',
            ei: '-23,5',
            s: '-0,02'
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'ph-блок', link: '/device--ph-block'}, {title: 'Параметры датчика'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col
                            xs4={4} sm={12} md={12} lg={12}
                        >
                            <div className="form-center">

                                <SelectField floatingLabelText="Тип датчика" value={1} disabled={true}>
                                    <MenuItem value={1} primaryText="pH-электрод" />
                                </SelectField><br/>
                                <TextField
                                    floatingLabelText="S/N датчика"
                                    defaultValue="123456"
                                    disabled={true}
                                /><br />

                                <KeyBoard
                                    value={this.state.pxi}
                                    floatingLabelText="pxi"
                                    onChange={(value)=>{this.onChangeInput('pxi', value)}}
                                />
                                <KeyBoard
                                    value={this.state.ei}
                                    floatingLabelText="Ei, мВ"
                                    onChange={(value)=>{this.onChangeInput('ei', value)}}
                                />

                                <TextField
                                    floatingLabelText="T, °С"
                                    defaultValue="20,3"
                                    disabled={true}
                                /><br />
                                <KeyBoard
                                    value={this.state.s}
                                    floatingLabelText="S, мВ/рХ"
                                    onChange={(value)=>{this.onChangeInput('s', value)}}
                                />

                                <br/><br/>
                                <RaisedButton
                                    label="Сохранить"
                                    primary={true}
                                /><br/>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhBlockParams);

import PropTypes from 'prop-types';
PhBlockParams.propTypes  = {
    //listData: PropTypes.array.isRequired
};
