/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, SelectField, MenuItem, Checkbox, Toggle, RaisedButton } from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './PhBlockCalibration.sass';

class PhBlockCalibration extends Component {

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'ph-блок', link: '/device--ph-block'}, {title: 'Калибровка ph-блока'}];
        return(
            <div className="phblock-calibration">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                        style={{overflowX: 'auto'}}
                    >
                        <div className="phblock-calibration__content">
                            <Row style={{padding: 15}}>
                                <Col style={{width: '22%'}}>
                                    <Position/>
                                </Col>
                                <Col style={{width: '16.6%'}}>
                                    <Buffer/>
                                </Col>
                                <Col style={{width: '14%'}}>
                                    <Isp/>
                                </Col>
                                <Col style={{width: '16.6%'}}>
                                    <OldVal/>
                                </Col>
                                <Col style={{width: '16.6%'}}>
                                    <NewVal/>
                                </Col>
                                <Col style={{width: '10%'}}>
                                    <Status/>
                                </Col>
                                <br/>
                                <Col style={{width: '100%'}}>
                                    <BottomSection/>
                                </Col>
                            </Row>
                        </div>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhBlockCalibration);

import PropTypes from 'prop-types';
PhBlockCalibration.propTypes  = {
    //listData: PropTypes.array.isRequired
};




function Position() {
    return <div>
        <Subheader>№ позиции</Subheader>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="1" />
        </SelectField>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="-" />
        </SelectField>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="2" />
        </SelectField>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="-" />
        </SelectField>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="3" />
        </SelectField>
        <SelectField
            value={1}
            style={{width: 100}}
        >
            <MenuItem value={1} primaryText="4" />
        </SelectField>
    </div>
}

function Buffer() {
    const style={
        height: 43
    };
    return <div>
        <Subheader>Буфер</Subheader>
        <p style={style}>1,64</p>
        <p style={style}>4,01</p>
        <p style={style}>7,01</p>
        <p style={style}>10,01</p>
        <p style={style}>12,45</p>
        <p style={style}>Промывка</p>
    </div>
}

function Isp() {
    const style = {
        width: 20,
        marginLeft: 15,
        marginTop: 10,
        height: 40
    };
        return <div>
        <Subheader>Исп</Subheader>
        <Checkbox
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
    </div>
}

function OldVal() {
    const style={
        height: 43
    };
    return <div>
        <Subheader>Стар</Subheader>
        <p style={style}>-0,150</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
    </div>
}

function NewVal() {
    const style={
        height: 43
    };
    return <div>
        <Subheader>Нов</Subheader>
        <p style={style}>-0,145</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
        <p style={style}>0,0</p>
    </div>
}

function Status() {
    const style = {
        width: 20,
        marginLeft: 15,
        marginTop: 10,
        height: 40
    };
    return <div>
        <Subheader>Статус</Subheader>
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={true}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
        <Checkbox
            defaultChecked={false}
            style={style}
        />
    </div>
}

function BottomSection() {

    const onSave = ()=>{
        toastr.success('Сохранение', 'Параметры сохранены');
    }
    
    const style = {
        p : {
          margin: 0,
          marginLeft: 15
        },
        btn: {
            margin: 15
        }
    };
    return <div>
        <Row style={{padding: 15}}>
            <Col style={{width: '33%'}}>
                <Subheader>Датчик</Subheader>
                <p style={style.p}>pH водный</p>
            </Col>
            <Col style={{width: '33%'}}>
                <Subheader>Дата калибровки</Subheader>
                <p style={style.p}>00.00.00</p>
            </Col>
            <Col style={{width: '33%'}}>
                <Subheader>Наклон:</Subheader>
                <p style={style.p}>nan мВ/pH</p>
            </Col>
        </Row>
        <RaisedButton
            label='График калибровки'
            style={style.btn}
        />
        <SelectField
            value={1}
            style={{top: 25}}
        >
            <MenuItem value={1} primaryText="Набор буферов 1" />
            <MenuItem value={2} primaryText="Набор буферов 2" />
            <MenuItem value={3} primaryText="Набор буферов 3" />
            <MenuItem value={0} primaryText="Новый" />
        </SelectField>
        <br/>
        <RaisedButton
            label='Калибровать'
            primary={true}
            style={style.btn}
        />
        <RaisedButton
            label='Сохранить'
            primary={true}
            style={style.btn}
            onClick={onSave}
        />
    </div>
}