/**
 * Created by i.chernyakov on 15.02.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, DropDownMenu, RaisedButton, Checkbox} from 'material-ui';

import {TopBar, KeyBoard} from 'blocks';
import {style} from 'config';


class PhBlockElectrode extends Component {
    constructor(props){
        super(props);
        this.state = {
            type: 1,
            termo_sensor: 1,
            mode_stabilizations: '0',
            name_sensor: 'ph водный',
            sn_sensor: '11111',
            sn_sensor_new: '',
            sn_termo_sensor: '11111',
            reference_electrode: '0',
            sn_reference_electrode: '11111',
            sn_reference_electrode_new: '',
            sn_termo_sensor_new: '',
            range_steepness: '41-62',
            asymmetry: '25',
            isopotential_point: '',
            isopotential_point_1: '',
            time: '',
            saved_calibration: 0,
            isShowParams: true
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    onSave() {
        toastr.success('Сохранение', 'Параметры сохранены');
    }

    onShowParams() {
        this.setState({isShowParams: !this.state.isShowParams});
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'ph-блок', link: '/device--ph-block'}, {title: 'электрод'}];
        const isNewSnSensor = this.state.sn_sensor == '0' ? true : false;
        const isShowParams = this.state.isShowParams;
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} xs8={8} sm={12} md={12} lg={12}>
                            {!isShowParams &&
                            <Row>
                                <Col
                                    xs4={4} xs8={4} sm={6} md={5} mdOffset={1} lg={4} lgOffset={2}
                                >
                                    <div className="form-center">
                                        <KeyBoard
                                            value={this.state.name_sensor}
                                            floatingLabelText="Имя датчика"
                                            onChange={(value)=>{this.onChangeInput('name_sensor', value)}}
                                        />

                                        <SelectField
                                            floatingLabelText="S/N датчика"
                                            value={this.state.sn_sensor}
                                            onChange={(a,b,value)=>{this.onChangeInput('sn_sensor', value)}}
                                        >
                                            <MenuItem value={'11111'} primaryText="11111" />
                                            <MenuItem value={'22222'} primaryText="22222" />
                                            <MenuItem value={'33333'} primaryText="33333" />
                                            <MenuItem value={'0'} primaryText="Новый" />
                                        </SelectField><br/>
                                        {isNewSnSensor &&
                                        <KeyBoard
                                            value={this.state.sn_sensor_new}
                                            floatingLabelText="Новый S/N датчика"
                                            onChange={(value)=>{this.onChangeInput('sn_sensor_new', value)}}
                                        />
                                        }
                                        <br/>
                                        <Checkbox
                                            label="Подключен"
                                        />


                                    </div>
                                </Col>
                                <Col
                                    xs4={4} xs8={4} sm={6} md={5} lg={4}
                                >
                                    <div className="form-center">

                                        <SelectField
                                            floatingLabelText="Тип датчика"
                                            value={this.state.type}
                                            onChange={(a,b,value)=>{this.onChangeInput('type', value)}}
                                        >
                                            <MenuItem value={1} primaryText="pH-электрод" />
                                            <MenuItem value={2} primaryText="IS-электрод" />
                                            <MenuItem value={3} primaryText="RedOx-электрод" />
                                        </SelectField>

                                        <div>
                                            <SelectField
                                                floatingLabelText="Режим стабилизации показаний"
                                                value={this.state.mode_stabilizations}
                                                onChange={(a,b,value)=>{this.onChangeInput('mode_stabilizations', value)}}
                                            >
                                                <MenuItem value={'0'} primaryText="Ручной" />
                                                <MenuItem value={'1'} primaryText="Автоматический" />
                                                <MenuItem value={'2'} primaryText="По временной константе" />
                                            </SelectField>

                                            {this.state.mode_stabilizations === '2' &&
                                            <KeyBoard
                                                value={this.state.time}
                                                onChange={(value)=>{this.onChangeInput('time', value)}}
                                                floatingLabelText="Время, сек [2-60]"
                                            />
                                            }
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            }
                            {isShowParams &&
                            <Row>
                                <Col
                                    xs4={4} xs8={4} sm={6} md={5} mdOffset={1} lg={4} lgOffset={2}
                                >
                                    <div className="form-center">

                                        <KeyBoard
                                            value={this.state.range_steepness}
                                            onChange={(value) => {
                                                this.onChangeInput('range_steepness', value)
                                            }}
                                            floatingLabelText="Дипазон крутизны, мВ\pH "
                                        />

                                        <KeyBoard
                                            value={this.state.asymmetry}
                                            onChange={(value) => {
                                                this.onChangeInput('asymmetry', value)
                                            }}
                                            floatingLabelText="Асимметрия, мВ"
                                        />
                                        <div>

                                        {this.state.reference_electrode == '1' &&
                                            <div>
                                                <SelectField
                                                    floatingLabelText="S/N электрода сравнения"
                                                    value={this.state.sn_reference_electrode}
                                                    onChange={(a, b, value) => {
                                                        this.onChangeInput('sn_reference_electrode', value)
                                                    }}
                                                >
                                                    <MenuItem value={'11111'} primaryText="11111"/>
                                                    <MenuItem value={'22222'} primaryText="22222"/>
                                                    <MenuItem value={'33333'} primaryText="33333"/>
                                                    <MenuItem value={'0'} primaryText="Новый"/>
                                                </SelectField><br/>
                                                {this.state.sn_reference_electrode == '0' &&
                                                    <KeyBoard
                                                        value={this.state.sn_reference_electrode_new}
                                                        floatingLabelText="Новый S/N электрода сравнения"
                                                        onChange={(value) => {
                                                            this.onChangeInput('sn_reference_electrode_new', value)
                                                        }}
                                                    />
                                                }
                                            </div>
                                            }
                                        </div>
                                    </div>
                                </Col>
                                <Col
                                    xs4={4} xs8={4} sm={6} md={5} lg={4}
                                >
                                    <div className="form-center">

                                        {this.state.type === 1 &&
                                        <div>
                                            <KeyBoard
                                                value={this.state.isopotential_point}
                                                onChange={(value) => {
                                                    this.onChangeInput('isopotential_point', value)
                                                }}
                                                floatingLabelText="Изопотенциальная точка, рХi"
                                            />
                                            <KeyBoard
                                                value={this.state.isopotential_point_1}
                                                onChange={(value) => {
                                                    this.onChangeInput('isopotential_point_1', value)
                                                }}
                                                floatingLabelText="Изопотенциальная точка, мВ"
                                            />
                                        </div>
                                        }


                                        {/*<div style={{marginLeft: -24, marginRight: -24}}>*/}
                                            {/*<DropDownMenu*/}
                                                {/*value={this.state.saved_calibration}*/}
                                                {/*onChange={(event, key, value) => {*/}
                                                    {/*this.onChangeInput('saved_calibration', value)*/}
                                                {/*}}*/}
                                                {/*autoWidth={false}*/}
                                                {/*style={{width: 256}}*/}
                                            {/*>*/}
                                                {/*<MenuItem value={0} primaryText="Сохраненные калибровки"/>*/}
                                                {/*<MenuItem value={1} primaryText="Калибровка 1"/>*/}
                                                {/*<MenuItem value={2} primaryText="Калибровка 2"/>*/}
                                                {/*<MenuItem value={3} primaryText="Калибровка 3"/>*/}
                                                {/*<MenuItem value={4} primaryText="Калибровка 4"/>*/}
                                                {/*<MenuItem value={5} primaryText="Калибровка 5"/>*/}
                                                {/*<MenuItem value={6} primaryText="Новая калибровка"/>*/}
                                            {/*</DropDownMenu>*/}
                                        {/*</div>*/}

                                    </div>
                                </Col>
                            </Row>
                            }

                            <br/><br/>
                            <Row center={['sm', 'md', 'lg']}>
                                <Col xs4={4} xs8={4} sm={12} md={12} lg={12}>
                                    {!isShowParams &&
                                        <div>
                                            <RaisedButton
                                                label={'Параметры электрода'}
                                                primary={true}
                                                style={{marginRight: 50}}
                                                onClick={this.onShowParams.bind(this)}
                                            />
                                            <Link to={'/device--ph-block--calibration'}>
                                                <RaisedButton
                                                    label={'Откалибровать'}
                                                    primary={true}
                                                    style={{marginRight: 50}}
                                                />
                                            </Link>
                                            <RaisedButton
                                                label={'Cохранить'}
                                                primary={true}
                                            />
                                        </div>
                                    }
                                    {isShowParams &&
                                        <div>
                                            <RaisedButton
                                                label={'Отменить'}
                                                primary={true}
                                                style={{marginRight: 50}}
                                                onClick={this.onShowParams.bind(this)}
                                            />
                                            <RaisedButton
                                                label={'Cохранить'}
                                                primary={true}
                                            />
                                        </div>
                                    }

                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhBlockElectrode);

import PropTypes from 'prop-types';
PhBlockElectrode.propTypes  = {
    //listData: PropTypes.array.isRequired
};
