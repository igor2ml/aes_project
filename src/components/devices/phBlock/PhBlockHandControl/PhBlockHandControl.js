/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, Subheader, RaisedButton} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';
import {getValueElectrode, getValueThermometer} from '../../../../actions/phActions';

class PhBlockHandControl extends Component {

    constructor(props){
        super(props);
        this.state = {
            electrode: 1, //todo нужно запрашивать список электродов из подключенных к системе
            calibration: 2 //todo нужно запрашивать список калибровок
        }
    }
    
    componentWillMount() {
        this.intervalID = null;
        this.permanentGetValue();
    }

    componentWillUnmount(){
        clearInterval(this.intervalID);
    }
    
    permanentGetValue() {
        this.intervalID = setInterval(()=>{
            this.props.pageActions.getValueElectrode();
            this.props.pageActions.getValueThermometer();
        }, 1000);
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    
    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'ph-блок', link: '/device--ph-block'}, {title: 'Ручное управление'}];
        const style = {
            subheader: {
                marginTop: 15,
                padding: 0
            },
            p : {
                margin: 0,
            },
            value: {
                fontSize: 32,
                margin: 0
            },
            btn: {
                marginTop: 30,
                marginRight: 15
            }
        };
        const {valueElectrode, valueThermometer} = this.props;
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">

                                <SelectField
                                    floatingLabelText="Электрод"
                                    onChange={(a, b, value)=>{this.onChangeInput('electrode', value)}}
                                    value={this.state.electrode}
                                >
                                    <MenuItem value={1} primaryText="pH водный" />
                                    <MenuItem value={2} primaryText="Термометр" />
                                </SelectField>

                                <SelectField
                                    floatingLabelText="Калибровка"
                                    onChange={(a, b, value)=>{this.onChangeInput('calibration', value)}}
                                    value={this.state.calibration}
                                >
                                    <MenuItem value={1} primaryText="Калибровка 1" />
                                    <MenuItem value={2} primaryText="Калибровка 2" />
                                </SelectField>

                                {this.state.electrode===1 &&
                                    <div>
                                        <SelectField
                                            floatingLabelText="Единицы измерения"
                                            value={2}
                                        >
                                            {/*<MenuItem value={1} primaryText="pH" />*/}
                                            <MenuItem value={2} primaryText="mV" />
                                        </SelectField>
                                        <Subheader
                                            style={style.subheader}
                                        >Результат измерения</Subheader>
                                        <p
                                            style={style.value}
                                        >{valueElectrode}</p>
                                    </div>
                                }

                                {this.state.electrode===2 &&
                                <div>
                                    <SelectField
                                        floatingLabelText="Единицы измерения"
                                        value={2}
                                    >
                                        {/*<MenuItem value={1} primaryText="pH" />*/}
                                        <MenuItem value={2} primaryText="mV" />
                                    </SelectField>
                                    <Subheader
                                        style={style.subheader}
                                    >Результат измерения</Subheader>
                                    <p
                                        style={style.value}
                                    >{valueThermometer}</p>
                                </div>
                                }
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        valueElectrode: state.devices.ph.electrode.value,
        valueThermometer: state.devices.ph.thermometer.value
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            getValueElectrode,
            getValueThermometer
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhBlockHandControl);

import PropTypes from 'prop-types';
PhBlockHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
};


