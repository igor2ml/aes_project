/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, TextField, RaisedButton} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './Libra.sass';

class Libra extends Component {

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Весы', link: '/device--libra'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <Content/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Libra);

import PropTypes from 'prop-types';
Libra.propTypes  = {
    //listData: PropTypes.array.isRequired
};



function Content() {
    const onSave = ()=>{
        toastr.success('Сохранение', 'Параметры сохранены');
    };
    return <div className="form-center">
        <TextField
            floatingLabelText="Устройство"
            value={'Весы'}
            disabled={true}
        /><br />
        <SelectField
            floatingLabelText="Тип весов"
            value={2}
        >
            <MenuItem value={1} primaryText="НЕТ" />
            <MenuItem value={2} primaryText="OHAUS" />
            <MenuItem value={3} primaryText="Mettler Toledo" />
            <MenuItem value={4} primaryText="SARTORIUS" />
        </SelectField>
        <br /><br/>
        <RaisedButton
            label={'Сохранить'}
            primary={true}
            onClick={onSave}
        />

    </div>
}