/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, TextField, RaisedButton, SelectField, MenuItem, Checkbox} from 'material-ui';

import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';

import './PumpCalibration.sass';

class PumpCalibration extends Component {

    constructor(props){
        super(props);
        this.state = {
            specific_volume_water: '1,001'
        }
    }
    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Перистальтический насос', link: '/device--pump'}, {title: 'Калибровка Насос 1'}];
        return(
            <div className="phblock-calibration">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} xs8={8} sm={12} md={10} lg={10} mdOffset={1} lgOffset={1}>
                            <Row center={['sm', 'md', 'lg']}>
                                <Col sm={6} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Промывка'}
                                    />
                                </Col>
                                <Col sm={6} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Калибровка'}
                                    />
                                </Col>
                            </Row>
                            <br/>
                            <Row>
                                <Col sm={6} md={6} lg={6}>
                                    <p>Введите текущую температуру <br/>  и атмосферное давление:</p>
                                    <Row>
                                        <Col sm={6} md={4} lg={4}>
                                            <KeyBoard
                                                value={''}
                                                hintText="T° = 25"
                                                floatingLabelText="T° = 25"
                                                onChange={(value)=>{this.onChangeInput('t', value)}}
                                                style={{width: 120}}
                                            />
                                        </Col>
                                        <Col sm={6} md={6} lg={6}>
                                            <KeyBoard
                                                value={''}
                                                hintText="p = 760 мм"
                                                floatingLabelText="p = 760 мм"
                                                onChange={(value)=>{this.onChangeInput('p', value)}}
                                                style={{width: 120}}
                                            />
                                        </Col>
                                    </Row>
                                    <p>Или введите удельный объем воды <br/> при текущих температуре <br/>  и атмосферном давлении, см3/г:</p>
                                    <KeyBoard
                                        value={''}
                                        hintText="1,001"
                                        onChange={(value)=>{this.onChangeInput('v', value)}}
                                        style={{width: 120}}
                                    />
                                </Col>
                                <Col>
                                    <Row>
                                        <Col sm={5} md={5} lg={5}>
                                            <p>№ взвешивания</p>
                                            <p style={{marginTop: 32, textAlign: 'center'}}>1</p>
                                            <p style={{marginTop: 30, textAlign: 'center'}}>2</p>
                                            <p style={{marginTop: 30, textAlign: 'center'}}>3</p>
                                        </Col>
                                        <Col sm={5} md={5} lg={5}>
                                            <p>Масса, г</p>
                                            <KeyBoard
                                                value={''}
                                                hintText="29,9"
                                                onChange={(value)=>{this.onChangeInput('m1', value)}}
                                                style={{width: 120}}
                                            />
                                            <KeyBoard
                                                value={''}
                                                hintText="59,34"
                                                onChange={(value)=>{this.onChangeInput('m2', value)}}
                                                style={{width: 120}}
                                            />
                                            <KeyBoard
                                                value={''}
                                                hintText="88,95"
                                                onChange={(value)=>{this.onChangeInput('m3', value)}}
                                                style={{width: 120}}
                                            />
                                        </Col>
                                        <Col sm={2} md={2} lg={2}>
                                            <Checkbox style={{marginTop: 62}}/>
                                            <Checkbox style={{marginTop: 27}}/>
                                            <Checkbox style={{marginTop: 27}}/>
                                        </Col>
                                    </Row>
                                    <p>Статус калибровки:</p>
                                </Col>
                            </Row>
                            <br/>
                            <Row center={['sm', 'md', 'lg']}>
                                <Col xs4={4} xs8={8} sm={6} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Отменить'}
                                        primary={true}
                                        onClick={()=>{this.props.history.goBack()}}
                                    />
                                </Col>
                                <Col xs4={4} xs8={8} sm={6} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PumpCalibration);

import PropTypes from 'prop-types';
PumpCalibration.propTypes  = {
    //listData: PropTypes.array.isRequired
};



function Weighing() {
    const style={
        height: 35,
        paddingLeft: 60
    };
    return <div>
        <Subheader>№ взвешивания</Subheader>
        <p style={style}>1</p>
        <p style={style}>2</p>
        <p style={style}>3</p>
    </div>
}

function Mass() {
    const style = {
        width: 110
    };

    return <div>
        <Subheader>масса, г</Subheader>
        <TextField
            value={'29,99'}
            disabled={true}
            style={style}
            name={'1'}
        /><br/>
        <TextField
            value={'59,34'}
            disabled={true}
            style={style}
            name={'2'}
        /><br/>
        <TextField
            value={'88,95'}
            disabled={true}
            style={style}
            name={'3'}
        />
    </div>
}

function Volume(props) {
    return <div>
        <KeyBoard
            value={props.state.specific_volume_water}
            floatingLabelText="Удельный объем воды при текущих температуре и атмосферном давлении, см3/г"
            onChange={(value)=>{props.onChangeInput('specific_volume_water', value)}}
            floatingLabelStyle={{top: 5}}
            style={{marginTop: 40}}
        />
    </div>
}

function BottomSection() {
    const onSave = ()=>{
        toastr.success('Сохранение', 'Параметры сохранены');
    };
    const style = {
        p : {
            margin: 0,
            marginLeft: 15
        },
        btn: {
            margin: 15
        }
    };
    return <div>
        <Row style={{padding: 15}}>
            <Col style={{width: '33%'}}>
                <Subheader>Дата калибровки</Subheader>
                <p style={style.p}>00.00.00</p>
            </Col>
            <Col style={{width: '33%'}}>
                <Subheader>Максимальная скорость</Subheader>
                <p style={style.p}>30 мл/мин</p>
            </Col>
            <Col style={{width: '33%'}}>
                <Subheader>Тип весов</Subheader>
                <SelectField
                    value={1}
                    style={{width: 130}}
                >
                    <MenuItem value={1} primaryText="QHAUS" />
                    <MenuItem value={2} primaryText="QHAUS" />
                </SelectField>

            </Col>
        </Row>
        <RaisedButton
            label='Ручная калибровка'
            style={style.btn}
        />
        <RaisedButton
            label='Автоматическая калибровка'
            style={style.btn}
        />
        <br/>

        <RaisedButton
            label='Сохранить'
            primary={true}
            style={style.btn}
            onClick={onSave}
        />
    </div>
}
