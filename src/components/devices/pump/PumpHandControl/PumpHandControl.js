/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, RaisedButton, TextField, Checkbox} from 'material-ui';
import {CardTitle, CardHeader} from 'material-ui/Card';

import {TopBar} from 'blocks';
import {style} from 'config';


class Content extends Component {
    constructor(props){
        super(props);
        this.state = {
            add_volume: ''
        }
    }
    onWash() {
        toastr.success('Промывка', 'Промывка жидкостного тракта');
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    render(){
        return <Row>
            <Col xs4={4} xs8={8} sm={12} md={10} lg={10} mdOffset={1} lgOffset={1}>
                <Row>
                    <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                        <CardTitle
                            title={'Добавить объем, мл'}
                            style={{padding: 0}}
                        />
                        <br/>
                        <TextField
                            hintText="Объем"
                            floatingLabelText="Объем"
                            value={this.state.add_volume}
                            onChange={(e, value)=>{this.onChangeInput('add_volume',value)}}
                            type={'number'}
                        />
                        <br/><br/>
                        <RaisedButton
                            label={'Добавить'}
                        />
                    </Col>
                    <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                        <CardTitle
                            title={'Ручное управление'}
                            style={{padding: 0}}
                        />
                        <br/>
                        <RaisedButton
                            label={'Старт'}
                        />
                        <br/><br/>
                        <RaisedButton
                            label={'Стоп'}
                        />
                        <br/><br/>
                        <Checkbox
                            label={'Обратное направление'}
                        />
                    </Col>
                </Row>
                <br/><br/>
                <Row>
                    <Col xs4={4} xs8={8} sm={12} md={4} lg={4}>
                        <RaisedButton
                            label={'Промывка'}
                            primary={true}
                            onClick={this.onWash.bind(this)}
                            style={{width: 200, height: 100}}
                            labelStyle={{fontSize: 20}}
                        />
                    </Col>
                    <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                        <p>Автоматическая промывка жидкостного тракта на максимальной скорости в течение минуты, удаление пузырьков</p>
                    </Col>
                </Row>
            </Col>
        </Row>
    }
}


class PumpHandControl extends Component {

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Перистальтический насос', link: '/device--pump'}, {title: 'Ручное управление'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <Content/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PumpHandControl);

import PropTypes from 'prop-types';
PumpHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
};
