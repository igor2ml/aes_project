/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';
import {withRouter} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {FlatButton, RaisedButton, Tabs, Tab} from 'material-ui';
import {CardHeader} from 'material-ui/Card';

import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';

import './Pump.sass';

class PumpItemComponent extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: this.props.name,
            reagent: 'PPV'
        }
    }
    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    onSave() {
        toastr.success('Сохранение', 'Параметры сохранены');
    }

    onCancel() {
        this.props.history.goBack();
    }

    render() {
        console.log(this.props);
        return <Row>
            <Col xs4={4} xs8={8} sm={12} md={10} lg={12} mdOffset={1} lgOffset={1}>
                <br/><br/>
                <Row>
                    <Col sm={12} md={6} lg={6}>
                        <KeyBoard
                            value={this.state.name}
                            hintText="Имя"
                            floatingLabelText="Имя"
                            onChange={(value)=>{this.onChangeInput('name', value)}}
                        />
                        <KeyBoard
                            value={this.state.reagent}
                            hintText="Реагент"
                            floatingLabelText="Реагент"
                            onChange={(value)=>{this.onChangeInput('reagent', value)}}
                        />
                        <CardHeader
                            title={'Дата калибровки'}
                            subtitle={'12.05.2018'}
                        />
                    </Col>
                    <Col sm={12} md={6} lg={6}>
                        <br/><br/>
                        <Link to={'/device--pump--hand-control'}>
                            <RaisedButton
                                label={'Ручное управление'}
                            />
                        </Link>
                        <br/><br/><br/>
                        <Link to={'/device--pump--calibration'}>
                            <RaisedButton
                                label={'калибровка'}
                            />
                        </Link>
                    </Col>
                </Row>
                <br/><br/>
                <Row center={['sm', 'md', 'lg']}>
                    <Col sm={12} md={6} lg={6}>
                        <RaisedButton
                            label={'Отмена'}
                            primary={true}
                            onClick={this.onCancel.bind(this)}
                        />
                    </Col>
                    <Col sm={12} md={6} lg={6}>
                        <RaisedButton
                            label={'Сохранить'}
                            primary={true}
                            onClick={this.onSave.bind(this)}
                        />
                    </Col>
                </Row>
            </Col>
        </Row>
    }
};
const PumpItem = withRouter(PumpItemComponent);

class Pump extends Component {

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Перистальтический насос', link: '/device--pump'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                            <br/>
                            <Tabs>
                                <Tab label="Насос 1" >
                                    <div>
                                        <PumpItem
                                            name={'Насос 1 PPV'}
                                        />
                                    </div>
                                </Tab>
                                <Tab label="Насос 2" >
                                    <div>
                                        <PumpItem
                                            name={'Насос 2 PPV'}
                                        />
                                    </div>
                                </Tab>
                                <Tab
                                    label="Насос 3"
                                >
                                    <div>
                                        <PumpItem
                                            name={'Насос 3 PPV'}
                                        />
                                    </div>
                                </Tab>
                            </Tabs>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pump);

import PropTypes from 'prop-types';
Pump.propTypes  = {
    //listData: PropTypes.array.isRequired
};

