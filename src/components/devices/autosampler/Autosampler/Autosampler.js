/**
 * Created by i.chernyakov on 15.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {
    RaisedButton
} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';


class Autosampler extends Component {

    render() {
        const breadCrumbs = [{title: 'Главное меню'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <RaisedButton
                                label={'Конфигурация'}
                                primary={true}
                                onClick={()=>{this.props.history.push('/device--autosampler-config')}}
                                style={{margin: 50, height: 80}}
                            />
                            <RaisedButton
                                label={'Ручное управление'}
                                primary={true}
                                onClick={()=>{this.props.history.push('/device--autosampler--hand-control')}}
                                style={{margin: 50, height: 80}}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Autosampler));

import PropTypes from 'prop-types';
Autosampler.propTypes  = {
    //listData: PropTypes.array.isRequired
};