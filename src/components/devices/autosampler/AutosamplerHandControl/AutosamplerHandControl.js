import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, RaisedButton, FloatingActionButton } from 'material-ui';
import IconUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import IconDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import {TopBar} from 'blocks';
import {style} from 'config';
import AutosamplerView from '../common/AutosamplerView/AutosamplerView';
import {setPositionLift} from '../../../../actions/autosampleActions';

class AutosamplerHandControl extends Component {

    constructor(props){
        super(props);
        this.state = {
            listConfig: [
                {position: 1, value: 'Образец', isActive: false},
                {position: 2, value: 'Промывка ', isActive: false},
                {position: 3, value: 'Буфер ', isActive: false},
                {position: 4, value: 'Образец', isActive: false},
                {position: 5, value: 'Образец', isActive: true},
                {position: 6, value: 'Промывка ', isActive: false},
                {position: 7, value: 'Буфер', isActive: false},
                {position: 8, value: 'Образец', isActive: false},
                {position: 9, value: 'Промывка ', isActive: false},
                {position: 10, value: 'Стандарт', isActive: false},
                {position: 11, value: 'Стандарт', isActive: false},
                {position: 12, value: 'Титр ', isActive: false},
                {position: 13, value: 'Стандарт', isActive: false},
                {position: 14, value: 'Цвет', isActive: false},
                {position: 15, value: 'Образец', isActive: false}
            ]
        }
    }

    setPositionLift(position) {
        //this.props.pageActions.setPositionLift(position);
    }

    render() {
        const styles = {
            chip: {
                margin: 4,

            },
            wrapperChip: {
                display: 'flex',
                flexWrap: 'wrap',
            },
        };
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Автосамплер', link: '/device--autosampler'}, {title: 'Ручное управление'}];
        const isChangingPositionAutosample = this.props.isChangingPosition;
        const liftPosition = this.props.liftPosition;
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col
                            lg={3} md={4} sm={4} xs8={4} lgOffset={1} mdOffset={1}
                        >
                            <p>Выберите позицию</p>
                            <AutosamplerView/>
                        </Col>
                        <Col
                            lg={4} md={4} sm={4}
                        >
                            <p>Позиционировании датчиков</p>
                            {!isChangingPositionAutosample &&
                                <div>
                                    <FloatingActionButton
                                        onClick={()=>{this.setPositionLift(0)}}
                                        secondary={liftPosition===0 ? true : false}
                                    >
                                        <IconUp/>
                                    </FloatingActionButton>

                                    <p>Лифт</p>

                                    <FloatingActionButton
                                        onClick={()=>{this.setPositionLift(1)}}
                                        // backgroundColor='#fff'
                                        // secondary={liftPosition===1 ? true : false}
                                    >
                                        <IconDown/>
                                    </FloatingActionButton>
                                </div>
                            }
                            {isChangingPositionAutosample &&
                                <p>В данный момент позиционирование лифта невозможно</p>
                            }

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        isChangingPosition: state.devices.autosampler.isChangingPosition,
        liftPosition: state.devices.autosampler.liftPosition
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            setPositionLift
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutosamplerHandControl);

import PropTypes from 'prop-types';
AutosamplerHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
};



