/**
 * Created by i.chernyakov on 15.11.2017.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';

import './AutosamplerView.sass';

import {setPositionAutosample, getStatusAutosample} from '../../../../../actions/autosampleActions';


class AutosamplerView extends Component {

    constructor(props){
        super(props);
        this.state = {
            radius: 100,
            radiusCell: 12
        }
    }

    componentDidUpdate(){
        //this.getStatus();
    }

    getStatus(){
        const isChangingPosition = this.props.isChangingPosition;
        let intervalID;
        if (isChangingPosition) {
            let requestCount = 0;
            intervalID = setInterval(()=>{
                this.props.pageActions.getStatusAutosample();
                requestCount++;
                if(requestCount > 12) {
                    clearInterval(intervalID);
                }
            }, 1000);
        } else {
            if(intervalID) clearInterval(intervalID);
        }
    }

    /**
     * расчет координат отображения ячейки относительно общего основания
     * @param i - номер ячейки для которой хотим расчитать позиционирование (x, y)
     * @param n - общее количество ячеек
     * @param r - радиус окружности
     */
    calcView(i, n, r){
        const number = n-i;
        const a = 360/n; //угол между ячейками по окружности
        const ai = a*number*Math.PI/180; //[радианы]
        const x = Math.sin(ai)*r+r;
        const y = Math.cos(ai)*r+r;
        return {x, y};
    }

    getListCell() {
        //пока! - id - для позиционирования, address - № колбы (ячейки)
        return [
            // {id: 1, address: 0},{id: 2, address: 11},{id: 3, address: 10},{id: 4, address: 9},{id: 5, address: 8},{id: 6, address: 7},{id: 7, address: 6},{id: 8, address: 5},{id: 9, address: 4},{id: 10, address: 3},{id: 11, address: 2},{id: 12, address: 1}
            {id: 0, address: 6},
            {id: 1, address: 7},
            {id: 2, address: 8},
            {id: 3, address: 9},
            {id: 4, address: 10},
            {id: 5, address: 11},
            {id: 6, address: 0},
            {id: 7, address: 1},
            {id: 8, address: 2},
            {id: 9, address: 3},
            {id: 10, address: 4},
            {id: 11, address: 5},
        ];
    }

    onClickCell(position) {
        //this.props.pageActions.setPositionAutosample(position);
    }

    render() {
        const listCell = this.getListCell();
        const commonNumber = listCell.length;
        const radius = this.state.radius;
        const radiusCell = this.state.radiusCell;

        const position = this.props.position;

        return(
            <div className="autosampler-view"
                 style={{width: `${radius*2+30}px`, height: `${radius*2+30}px`}}
            >
                <div className="autosampler-view__circle"
                     style={{width: `${radius*2}px`, height: `${radius*2}px`}}
                >
                    {
                        listCell.map((item)=>{
                            const geometryPosition = this.calcView(item.id, commonNumber, radius);
                            const classNmItem = item.address==position
                                ? 'autosampler-view__item autosampler-view__item_active'
                                : 'autosampler-view__item';
                            return <div
                                className={classNmItem}
                                key={item.id}
                                style={{
                                    left: `${geometryPosition.x-radiusCell}px`,
                                    top: `${geometryPosition.y-radiusCell}px`,
                                    width: `${radiusCell*2}px`,
                                    height: `${radiusCell*2}px`
                                }}
                                onClick={()=>{this.onClickCell(item.id)}}
                            >

                            </div>
                        })
                    }

                    <div className="autosampler-view__circle-inner">
                        {
                            listCell.map((item)=>{
                                const radiusInner = radius - 25;
                                const position = this.calcView(item.id, commonNumber, radiusInner);
                                return <div
                                    className="autosampler-view__number"
                                    key={item.id}
                                    style={{
                                        left: `${position.x}px`,
                                        top: `${position.y+16}px`,
                                    }}
                                >
                                    {item.address}
                                </div>
                            })
                        }
                    </div>

                </div>



            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        position: state.devices.autosampler.position,
        isChangingPosition: state.devices.autosampler.isChangingPosition
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            setPositionAutosample,
            getStatusAutosample
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutosamplerView);

AutosamplerView.propTypes  = {
    //listData: PropTypes.array.isRequired
};
