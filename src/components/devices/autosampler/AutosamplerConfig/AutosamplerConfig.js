/**
 * Created by i.chernyakov on 15.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, RaisedButton} from 'material-ui';
import {CardHeader} from 'material-ui/Card';
import {TopBar} from 'blocks';
import {style} from 'config';
import KeyBoard from '../../../blocks/KeyBoard/KeyBoard';
import AutosamplerView from '../common/AutosamplerView/AutosamplerView';

class AutosamplerConfig extends Component {

    constructor(props){
        super(props);
        this.state = {
            number: '121',
            type: 1,
            name: '1',
            party: '1',
            volume: '12',
            mass: '10',
            position: 1
            // time: '',
            // ph: '',
            // value: '',
            // unit: '',
            // color: ''
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }


    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Автосамплер', link: '/device--autosampler'}];

        return(
            <div className="autosampler-config">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        className={'content'}
                        middle={['xs4', 'sm', 'md', 'ld']}
                    >
                        <Col lg={12} md={12} sm={12} xs8={4} xs4={4}>
                            <Row
                                //center={['xs4', 'sm', 'md', 'ld']}
                                middle={['xs4', 'sm', 'md', 'ld']}
                            >
                                <Col
                                    lg={5} md={5} sm={12} xs8={4} xs4={4}
                                >
                                    <Row center={['xs4', 'sm', 'md', 'ld']}>
                                        <Col
                                            lg={12} md={12} sm={12} xs8={4} xs4={4}
                                        >
                                            <p>Конфигурация автосамплера</p>
                                            <br/>
                                        </Col>
                                    </Row>

                                    <AutosamplerView/>
                                </Col>
                                <Col
                                    lg={7} md={7} sm={12} xs8={4} xs4={4}
                                >
                                    <CardHeader
                                        title={'Информация о выбранной позиции'}
                                        style={{padding: 0}}
                                    />
                                    <Row>
                                        <Col
                                            lg={6} md={6} sm={12} xs8={4} xs4={4}
                                        >
                                            <SelectField
                                                floatingLabelText="Тип"
                                                value={this.state.type}
                                                onChange={(a, b, value)=>{
                                                    this.onChangeInput('type', value)}}
                                                style={{width: 200}}
                                            >
                                                <MenuItem value={1} primaryText="Образец" />
                                                <MenuItem value={2} primaryText="Промывка" />
                                                <MenuItem value={3} primaryText="Буфер" />
                                                <MenuItem value={4} primaryText="Стандарт" />
                                                <MenuItem value={5} primaryText="Титр" />
                                                <MenuItem value={6} primaryText="Цвет" />
                                            </SelectField>
                                            <br/>
                                            <KeyBoard
                                                value={this.state.name}
                                                hintText="Имя"
                                                floatingLabelText="Имя"
                                                onChange={(value)=>{this.onChangeInput('name', value)}}
                                                style={{width: 200}}
                                            />
                                            <KeyBoard
                                                value={this.state.party}
                                                hintText="Партия"
                                                floatingLabelText="Партия"
                                                onChange={(value)=>{this.onChangeInput('party', value)}}
                                                style={{width: 200}}
                                            />
                                        </Col>
                                        <Col
                                            lg={6} md={6} sm={12} xs8={4} xs4={4}
                                        >
                                            <KeyBoard
                                                value={this.state.volume}
                                                hintText="V, мл"
                                                floatingLabelText="V, мл"
                                                onChange={(value)=>{this.onChangeInput('volume', value)}}
                                                style={{width: 200}}
                                            />
                                            <KeyBoard
                                                value={this.state.mass}
                                                hintText="m,r"
                                                floatingLabelText="m,r"
                                                onChange={(value)=>{this.onChangeInput('mass', value)}}
                                                style={{width: 200}}
                                            />
                                            <RaisedButton
                                                label={'Сохранить информацию'}
                                                style={{marginTop: 15}}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row
                                center={['xs4', 'sm', 'md', 'ld']}
                            >
                                <Col xs4={4} sm={12} md={12} lg={12}>
                                    <br/><br/>
                                    <RaisedButton
                                        label={'Отменить'}
                                        primary={true}
                                        onClick={()=>{this.props.history.goBack()}}
                                        style={{marginRight: 50, height: 80}}
                                    />
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                        style={{marginLeft: 50, height: 80}}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutosamplerConfig);

AutosamplerConfig.propTypes  = {
    //listData: PropTypes.array.isRequired
};


