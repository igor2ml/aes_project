/**
 * Created by i.chernyakov on 19.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link, withRouter} from 'react-router-dom';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton, Divider, Subheader} from 'material-ui';
import {ListItem} from 'material-ui/List';
import {CardHeader} from 'material-ui/Card';
import {TopBar, KeyBoard} from 'blocks';
import {style} from 'config';


class TermoSensor extends Component {

    onClickElectrode(){
        //this.props.history.push('/device--ph-block--electrode');
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'ph-блок', link: '/device--ph-block'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col
                            xs4={4} xs8={8} sm={12} md={12} lg={12}
                        >
                            <div className="form-center">
                                <p>Термодатчики</p>
                                <div
                                    style={{border: '1px solid rgb(224, 224, 224)'}}
                                >
                                    <div onClick={()=>{this.onClickElectrode()}}>
                                        <ListItem
                                            primaryText="Встроенный в pH - электрод"
                                        />
                                        <Divider/>
                                    </div>
                                    <div onClick={()=>{this.onClickElectrode()}}>
                                        <ListItem
                                            primaryText="Встроенный в pH - водный"
                                        />
                                        <Divider/>
                                    </div>
                                    <div onClick={()=>{this.onClickElectrode()}}>
                                        <ListItem
                                            primaryText="Внешний"
                                        />
                                    </div>
                                </div>
                                <br/><br/>
                                <Link to={'device--termo-sensor--new'}>
                                    <RaisedButton
                                        label="Добавить датчик"
                                        primary={true}
                                    />
                                </Link>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TermoSensor));

import PropTypes from 'prop-types';
TermoSensor.propTypes  = {
    //listData: PropTypes.array.isRequired
};