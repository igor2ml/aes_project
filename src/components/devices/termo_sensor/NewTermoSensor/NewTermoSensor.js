/**
 * Created by i.chernyakov on 15.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter, Link} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, Checkbox, RaisedButton} from 'material-ui';
import {TopBar, KeyBoard} from 'blocks';
import {style} from 'config';

class NewTermoSensor extends Component {

    constructor(props){
        super(props);
        this.state = {
            type: 0,
            name: ''
        }
    }

    onChangeInput(value, name) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    render() {
        const breadCrumbs = [{title: 'Устройства', link: '/device'}, {title: 'Термодатчик', link: '/device--termo-sensor'}, {title: 'Новый датчик', link: '/device--termo-sensor--new'}];
        const isIntegratedSensor = this.state.type===0;
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">
                                <SelectField
                                    floatingLabelText="Тип датчика"
                                    value={this.state.type}
                                    onChange={(e, value)=>{this.onChangeInput(value, 'type')}}
                                >
                                    <MenuItem value={0} primaryText="Встроенный" />
                                    <MenuItem value={1} primaryText="Внешний" />
                                </SelectField>
                                {isIntegratedSensor &&
                                    <SelectField
                                        floatingLabelText="Имя датчика"
                                        value={0}
                                        onChange={(e, value)=>{this.onChangeInput(value, 'name_select')}}
                                    >
                                        <MenuItem value={0} primaryText="Электрод №1" />
                                        <MenuItem value={1} primaryText="Электрод №2" />
                                    </SelectField>
                                }
                                {!isIntegratedSensor &&
                                    <KeyBoard
                                        value={this.state.name}
                                        hintText="Имя датчика"
                                        floatingLabelText="Имя датчика"
                                        onChange={(value)=>{this.onChangeInput('name', value)}}
                                    />
                                }
                                <br/>
                                <Checkbox
                                    label="Подключен"
                                />
                            </div>
                            <br/><br/><br/>
                            <Row center={['sm','md','lg']}>
                                <Col sm={12} md={4} lg={4}>
                                    <RaisedButton
                                        label={'Отмена'}
                                        primary={true}
                                        onClick={()=>{this.props.history.push('/')}}
                                    />
                                </Col>
                                <Col sm={12} md={4} lg={4}>
                                    <Link to={'/device--termo-sensor--calibration'}>
                                        <RaisedButton
                                            label={'Калибровка'}
                                        />
                                    </Link>
                                </Col>
                                <Col sm={12} md={4} lg={4}>
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NewTermoSensor));

import PropTypes from 'prop-types';
NewTermoSensor.propTypes  = {
    //listData: PropTypes.array.isRequired
};
