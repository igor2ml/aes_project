/**
 * Created by i.chernyakov on 15.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton} from 'material-ui';
import {CardTitle} from 'material-ui/Card';

import {TopBar, KeyBoard} from 'blocks';
import {style} from 'config';


class TermoSensorCalibration extends Component {

    render() {
        const breadCrumbs = [{title: 'Устройства', link: '/device'}, {title: 'Термодатчик', link: '/device--termo-sensor'}, {title: 'Калибровка', link: '/device--termo-sensor--calibration'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={10} lg={10} mdOffset={1} lgOffset={1}>
                            <CardTitle
                                title={'Введите температуру'}
                            />
                            <Row center={['xs4', 'sm', 'md', 'ld']}>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <KeyBoard
                                        value={''}
                                        hintText="Первое измерение"
                                        floatingLabelText="Первое измерение"
                                        onChange={(value)=>{this.onChangeInput('one', value)}}
                                    />
                                </Col>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <br/>
                                    <RaisedButton
                                        label={'Измерить'}
                                    />
                                </Col>
                            </Row>
                            <Row center={['xs4', 'sm', 'md', 'ld']}>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <KeyBoard
                                        value={''}
                                        hintText="Второе измерение"
                                        floatingLabelText="Второе измерение"
                                        onChange={(value)=>{this.onChangeInput('two', value)}}
                                    />
                                </Col>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <br/>
                                    <RaisedButton
                                        label={'Измерить'}
                                    />
                                </Col>
                            </Row>
                            <br/><br/>
                            <Row center={['xs4', 'sm', 'md', 'ld']}>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Отменить'}
                                        primary={true}
                                        onClick={()=>{this.props.history.goBack()}}
                                    />
                                </Col>
                                <Col xs4={4} xs8={8} sm={12} md={6} lg={6}>
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TermoSensorCalibration));

import PropTypes from 'prop-types';
TermoSensorCalibration.propTypes  = {
    //listData: PropTypes.array.isRequired
};
