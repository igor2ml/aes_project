/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {
    RaisedButton,
    Subheader
} from 'material-ui';

import {TopBar} from '../../../blocks';

import {getStatusAutosample, setSpeedStirrer} from '../../../../actions/autosampleActions';

class MagneticStirrerHandControl extends Component {
    constructor(props){
        super(props);
        this.state = {
            speed: '100'
        };
        this.styles = {
            btn: {margin: 16}
        }
    }

    componentWillMount() {
        this.getStatus();
    }

    onMix (){
        toastr.success('Перемешать', 'Начинается перемешивание');
    }

    getStatus(){
        this.props.pageActions.getStatusAutosample();
    }

    setSpeed(speed){
        this.props.pageActions.setSpeedStirrer(speed);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Магнитная мешалка', link: '/device--magnetic-stirrer'}, {title: 'Ручное управление'}];
        
        const {isready, speed} = this.props.autosamplerData;

        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div style={{maxWidth: 500, margin: 'auto', marginBottom: 30}}>
                                <Subheader>
                                    {isready ? 'Прибор готов к использованию' : 'Прибор не готов к использованию'}
                                </Subheader>

                                <RaisedButton
                                    label={'Стоп'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(0)}}
                                    disabled={!isready}
                                    secondary={speed === 0 ? true : false}
                                />
                                <RaisedButton
                                    label={'1'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(1)}}
                                    disabled={!isready}
                                    secondary={speed === 1 ? true : false}
                                />
                                <RaisedButton
                                    label={'2'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(2)}}
                                    disabled={!isready}
                                    secondary={speed === 2 ? true : false}
                                />
                                <RaisedButton
                                    label={'3'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(3)}}
                                    disabled={!isready}
                                    secondary={speed === 3 ? true : false}
                                />
                                <RaisedButton
                                    label={'4'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(4)}}
                                    disabled={!isready}
                                    secondary={speed === 4 ? true : false}
                                />
                                <RaisedButton
                                    label={'5'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(5)}}
                                    disabled={!isready}
                                    secondary={speed === 5 ? true : false}
                                />
                                <RaisedButton
                                    label={'6'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(6)}}
                                    disabled={!isready}
                                    secondary={speed === 6 ? true : false}
                                />
                                <RaisedButton
                                    label={'7'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(7)}}
                                    disabled={!isready}
                                    secondary={speed === 7 ? true : false}
                                />
                                <RaisedButton
                                    label={'8'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(8)}}
                                    disabled={!isready}
                                    secondary={speed === 8 ? true : false}
                                />
                                <RaisedButton
                                    label={'9'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(9)}}
                                    disabled={!isready}
                                    secondary={speed === 9 ? true : false}
                                />
                                <RaisedButton
                                    label={'10'}
                                    style={this.styles.btn}
                                    onClick={()=>{this.setSpeed(10)}}
                                    disabled={!isready}
                                    secondary={speed >= 10 ? true : false}
                                />
                            </div>
                            <div className="form-center">
                                <RaisedButton
                                    label={'Перемешать'}
                                    onClick={this.onMix.bind(this)}
                                    primary={true}
                                    disabled={!isready}
                                />
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        autosamplerData: state.devices.autosampler
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            getStatusAutosample,
            setSpeedStirrer
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MagneticStirrerHandControl);

//import PropTypes from 'prop-types';
//MagneticStirrerHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
//};


