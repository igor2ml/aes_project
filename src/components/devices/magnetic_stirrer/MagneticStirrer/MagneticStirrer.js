/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {SelectField, MenuItem, TextField, RaisedButton, Slider} from 'material-ui';
import {CardTitle} from 'material-ui/Card';

import {TopBar} from 'blocks';
import {style} from 'config';

import './MagneticStirrer.sass';

class MagneticStirrer extends Component {

    constructor(props){
        super(props);
        this.state = {
            sliderValue: 0
        }
    }

    onChangeSlider(sliderValue) {
        this.setState({sliderValue:sliderValue});
    }

    getValueSpeed() {
        const possibleSpeeds = [0, 107,113,121,129,139,150,163,179,198,222,253,293,348,429,558,800];
        return possibleSpeeds[this.state.sliderValue];
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Магнитная мешалка', link: '/device--magnetic-stirrer'}];
        const valueSpeed = this.getValueSpeed();
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={8} lg={8}>
                            <CardTitle
                                title={'Выберите скорость, об/мин:'}
                            />
                            <br/>
                            <Slider
                                defaultValue={0}
                                value={this.state.sliderValue}
                                min={0}
                                max={16}
                                step={1}
                                onChange={(e, value)=>{this.onChangeSlider(value)}}
                            />
                            <CardTitle
                                title={valueSpeed}
                            />
                            <br/>
                            <RaisedButton
                                label={'Стоп'}
                                primary={true}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MagneticStirrer);

import PropTypes from 'prop-types';
MagneticStirrer.propTypes  = {
    //listData: PropTypes.array.isRequired
};

