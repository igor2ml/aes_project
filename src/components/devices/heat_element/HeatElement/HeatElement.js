/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton} from 'material-ui';
import {CardHeader} from 'material-ui/Card';

import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';

import './HeatElement.sass';

class HeatElement extends Component {

    constructor(props){
        super(props);
        this.state = {
            max_t: '90'
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Нагревательный элемент', link: '/device--heat-element'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={10} lg={10} mdOffset={1} lgOffset={1}>
                            <Row>
                                <Col sm={12} md={6} lg={6}>
                                    <KeyBoard
                                        value={''}
                                        hintText="Макс T, °C"
                                        floatingLabelText={'Макс T, °C'}
                                        onChange={(value)=>{this.onChangeInput('max_t', value)}}
                                    />
                                    <br/><br/>
                                    <RaisedButton
                                        label={'Сохранить'}
                                        primary={true}
                                    />
                                </Col>
                                <Col sm={12} md={6} lg={6}>
                                    <KeyBoard
                                        value={''}
                                        hintText="Нагреть до, °C"
                                        floatingLabelText={'Нагреть до, °C'}
                                        onChange={(value)=>{this.onChangeInput('max_t', value)}}
                                    />
                                    <CardHeader
                                        title={'Текущая, °C'}
                                        subtitle={'21,3°C'}
                                    />
                                    <br/>
                                    <RaisedButton
                                        label={'Нагреть'}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeatElement);

import PropTypes from 'prop-types';
HeatElement.propTypes  = {
    //listData: PropTypes.array.isRequired
};
