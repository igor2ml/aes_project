/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {TextField, Subheader, RaisedButton} from 'material-ui';

import {TopBar, KeyBoard} from '../../../blocks';
import {style} from 'config';

import './HeatElementHandControl.sass';

class HeatElementHandControl extends Component {
    constructor(props){
        super(props);
        this.state = {
            heat: '40,5'
        }
    }
    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Нагревательный элемент', link: '/device--heat-element'}, {title: 'Ручное управление'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <Content
                                onChangeInput={(name, value)=>{this.onChangeInput(name, value)}}
                                state={this.state}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeatElementHandControl);

import PropTypes from 'prop-types';
HeatElementHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
};


function Content(props) {
    const onHeat = ()=>{
        toastr.success('Нагревание', 'Начало нагревания');
    };
    const style = {
        subheader: {
            padding: 0
        },
        p: {
            margin: 0
        }
    };
    return <div className="form-center">
        <KeyBoard
            value={props.state.heat}
            hintText="Нагреть до, °С"
            floatingLabelText="Нагреть до, °С"
            onChange={(value)=>{props.onChangeInput('heat', value)}}
        />
        <br/>
        <Subheader style={style.subheader}>
            Текущая, °С
        </Subheader>
        <p style={style.p}>21,1</p>
        <br/><br/>
        <RaisedButton
            label={'Нагреть'}
            primary={true}
            onClick={onHeat}
        />
    </div>
}