/**
 * Created by i.chernyakov on 21.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Subheader, RaisedButton} from 'material-ui';
import {CardHeader, CardTitle} from 'material-ui/Card';

import {TopBar} from 'blocks';
import {style} from 'config';
import {getValuePhotoblock} from '../../../../actions/photoblockActions';

class PhotoBlockHandControl extends Component {

    componentWillMount() {
        this.intervalID = null;
        this.permanentGetValue();
    }

    componentWillUnmount(){
        clearInterval(this.intervalID);
    }

    permanentGetValue() {
        this.props.pageActions.getValuePhotoblock(0);
        this.props.pageActions.getValuePhotoblock(1);
        this.props.pageActions.getValuePhotoblock(2);
        this.intervalID = setInterval(()=>{
            // this.props.pageActions.getValuePhotoblock(0);
            // this.props.pageActions.getValuePhotoblock(1);
            // this.props.pageActions.getValuePhotoblock(2);
        }, 1000);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}, {title: 'Фото-блок', link: '/device--photo-block'}];
        const photoblock = {
            red: this.props.red,
            green: this.props.green,
            blue: this.props.blue,
        };
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <CardTitle
                                title={'Фото-блок'}
                            />
                            <br/>
                            <div className="clearfix">
                                <div style={{width: '33%', float: 'left'}}>
                                    <CardTitle
                                        title={'Red'}
                                        titleColor={'red'}
                                    />
                                    <CardTitle
                                        title={photoblock.red}
                                    />
                                </div>
                                <div style={{width: '33%', float: 'left'}}>
                                    <CardTitle
                                        title={'Green'}
                                        titleColor={'green'}
                                    />
                                    <CardTitle
                                        title={photoblock.green}
                                    />
                                </div>
                                <div style={{width: '33%', float: 'left'}}>
                                    <CardTitle
                                        title={'Blue'}
                                        titleColor={'blue'}
                                    />
                                    <CardTitle
                                        title={photoblock.blue}
                                    />
                                </div>
                            </div>
                            <br/>
                            <RaisedButton
                                label={'Установка 0'}
                                primary={true}
                                style={{margin: 15}}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        red: state.devices.photoblock.red,
        green: state.devices.photoblock.green,
        blue: state.devices.photoblock.blue
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            getValuePhotoblock
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoBlockHandControl);

import PropTypes from 'prop-types';
//PhotoBlockHandControl.propTypes  = {
    //listData: PropTypes.array.isRequired
//};


/*
    function Content() {
    const style = {
        subheader: {
            padding: 0
        },
        p: {
            margin: 0
        },
        btn: {
            margin: 15
        }
    };
    return <div>
        <Subheader
            style={style.subheader}
        >
            5,3% разница
        </Subheader>
        <div
            style={{width: 120}}
        >
            <div style={{width: '70%', height: 15, backgroundColor: 'red'}}></div>
            <div style={{width: '30%', height: 15, backgroundColor: 'green'}}></div>
            <div style={{width: '40%', height: 15, backgroundColor: 'blue'}}></div>
        </div>
        <br/>
        <Subheader
            style={style.subheader}
        >
            Имя датчика
        </Subheader>
        <p style={style.p}>фототрод</p>
        <br/>
        <RaisedButton
            label={'Установка 0'}
            primary={true}
            style={style.btn}
        />
    </div>
}
*/