/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {List, ListItem, Divider} from 'material-ui';
import {TopBar} from 'blocks';
import {style} from 'config';


class AnalysisPage extends Component {

    render() {
        const breadCrumbs = [{title: 'Анализ'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">
                                <List>
                                    <Link to={'/analysis__example'} className="link-wrap">
                                        <ListItem
                                            primaryText={'Борная кислота'}
                                        />
                                        <Divider/>
                                    </Link>
                                </List>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalysisPage);

import PropTypes from 'prop-types';
AnalysisPage.propTypes  = {
    //listData: PropTypes.array.isRequired
};