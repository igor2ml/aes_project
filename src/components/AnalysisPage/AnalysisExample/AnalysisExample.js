/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {TopBar} from 'blocks';
import {style} from 'config';
import CurrentValue from './CurrentValue/CurrentValue';
import ExecutableProgram from './ExecutableProgram/ExecutableProgram';
import AnalysisButtons from './AnalysisButtons/AnalysisButtons';
import AnalysisProgress from './AnalysisProgress/AnalysisProgress';

class AnalysisExample extends Component {

    render() {
        const breadCrumbs = [{title: 'Главное меню'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        //middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <Row>
                                <Col xs4={4} xs8={8} sm={6} md={5} lg={4} mdOffset={1} lgOffset={2}>
                                    <CurrentValue/>
                                </Col>
                                <Col xs4={4} xs8={8} sm={6} md={5} lg={4}>
                                    <ExecutableProgram/>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs4={4} xs8={8} sm={8} md={8} lg={8}>
                                    <AnalysisButtons/>
                                </Col>
                                <Col xs4={4} xs8={8} sm={4} md={4} lg={4}>
                                    <AnalysisProgress/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalysisExample);

import PropTypes from 'prop-types';
AnalysisExample.propTypes  = {
    //listData: PropTypes.array.isRequired
};
