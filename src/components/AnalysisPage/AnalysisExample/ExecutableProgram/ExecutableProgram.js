/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Chip, Paper, List, ListItem, Divider} from 'material-ui';
import imgBottomTear from './img/bottom-tear.svg';
import imgTopTear from './img/top-tear.svg';

class ExecutableProgram extends Component {

    constructor(props){
        super(props);
        this.state = {
            isScrolledBottom: false,
            isScrolledTop: true
        }
    }

    onScroll() {
        const scrollTop = this.scrollElement.scrollTop;
        const offsetHeight = this.scrollElement.offsetHeight;
        const scrollHeight = this.scrollElement.scrollHeight;

        const isScrolledBottom = (scrollHeight - scrollTop - offsetHeight) < 10;
        const isScrolledTop = scrollTop<10;
        this.setState({isScrolledBottom, isScrolledTop});
    }
    
    render() {
        const queues = Array.isArray(this.props.queues) ? this.props.queues : [];
        const isScrolledBottom = this.state.isScrolledBottom;
        const isScrolledTop = this.state.isScrolledTop;
        return <div>
            <h3 style={{paddingLeft: 6, marginBottom: 0}}>Программа метода</h3>
            <img src={imgTopTear} alt="top-svg"
                 style={{
                     position: 'relative', top: 5,
                     opacity: isScrolledTop ? 0 : 1
                 }}
            />
            <div style={{height: 'calc(100vh - 202px)', overflowX: 'auto', padding: '3px 5px'}}
                onScroll={this.onScroll.bind(this)}
                ref={(scrollElement)=>{this.scrollElement = scrollElement}}
            >
                {
                    queues.map((queue, idx)=>{
                        const samples = Array.isArray(queue.samples) ? queue.samples : [];
                        const actions = Array.isArray(queue.actions) ? queue.actions : [];
                        return <Paper key={idx} style={{padding: 10, marginBottom: 5}}>
                            <h4 style={{margin: 0, marginBottom: 10}}>Для каждого из образцов</h4>
                            <div style={{display: 'flex', flexWrap: 'wrap'}}>
                                {
                                    samples.map((sample, idx)=>{
                                        return <Chip key={idx} style={{margin: 5, marginLeft: 0}}>
                                            {sample}
                                        </Chip>
                                    })
                                }
                            </div>
                            <h4 style={{margin: 0, marginBottom: 10}}>Выполнить</h4>
                            {
                                actions.map((action, idx)=>{
                                    return <div key={idx} style={{padding: 5, paddingLeft: 15}}>
                                        {action.title}
                                    </div>
                                })
                            }
                        </Paper>
                    })
                }

            </div>
            <img src={imgBottomTear} alt="bottom-svg"
                 style={{
                     position: 'relative', top: -5,
                     opacity: isScrolledBottom ? 0 : 1
                 }}
            />
        </div>
    }
}

function mapStateToProps (state) {
    return{
        queues: state.analyses.program.queues
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExecutableProgram);

//import PropTypes from 'prop-types';
//ExecutableProgram.propTypes  = {
    //listData: PropTypes.array.isRequired
//};
