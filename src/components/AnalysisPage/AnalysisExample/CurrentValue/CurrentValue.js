/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import imgTitrator from './img/titrator.png';

class CurrentValue extends Component {

    getCurrentValue() {
        return null;
    }

    render() {
        const currentValue = this.getCurrentValue();
        return <div style={{display: 'flex', alignItems: 'center', height: '100%'}}>
            {currentValue &&
                <div>
                    <p>наименование</p>
                    <h3>ЗНАЧЕНИЕ</h3>
                </div>
            }
            {!currentValue &&
                <div>
                    <img src={imgTitrator} alt="titrator" />
                </div>
            }
        </div>
    }
}

function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentValue);

//import PropTypes from 'prop-types';
//CurrentValue.propTypes  = {
    //listData: PropTypes.array.isRequired
//};