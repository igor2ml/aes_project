/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class AnalysisProgress extends Component {

    render() {
        return <div style={{paddingTop: 5}}>
            <p style={{margin: 0}}>Прогресс: 0%</p>
            <p style={{margin: 0}}>Время выполнения: 00:00:00</p>
        </div>
    }
}

function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalysisProgress);

//import PropTypes from 'prop-types';
//AnalysisProgress.propTypes  = {
    //listData: PropTypes.array.isRequired
//};
