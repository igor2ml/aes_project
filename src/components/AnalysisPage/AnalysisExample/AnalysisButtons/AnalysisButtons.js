/**
 * Created by i.chernyakov on 11.05.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {RaisedButton} from 'material-ui';

class AnalysisButtons extends Component {

    render() {
        return <div>
            <RaisedButton
                label={'Стоп'}
                disabled={true}
            />
            <RaisedButton
                label={'Текущие результаты'}
                primary={true}
            />
            <RaisedButton
                label={'Старт'}
                backgroundColor={'#4CB050'}
                labelColor={'#fff'}
            />
        </div>
    }
}

function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalysisButtons);

//import PropTypes from 'prop-types';
//AnalysisButtons.propTypes  = {
    //listData: PropTypes.array.isRequired
//};