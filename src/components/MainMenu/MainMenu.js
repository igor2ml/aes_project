/**
 * Created by i.chernyakov on 14.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton} from 'material-ui';

import {TopBar} from '../blocks/index';

import './MainMenu.sass';

class MainMenu extends Component {


    render() {
        const styleBnt = {
            margin: 15,
            width: 260,
            height: 130
        };
        const labelStyle = {
            fontSize: 20
        };
        const breadCrumbs = [{title: 'Автоматический титратор AT03-1'}];
        return(
            <div className="main-menu">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col>
                            <Link to={'/analysis'}>
                                <RaisedButton
                                    label='Анализ'
                                    primary={true}
                                    style={styleBnt}
                                    labelStyle={labelStyle}
                                />
                            </Link>
                            <Link to={'/methods--new'}>
                                <RaisedButton
                                    label="МБИ"
                                    primary={true}
                                    style={styleBnt}
                                    labelStyle={labelStyle}
                                />
                            </Link>
                            <br/>
                            <Link to={'/'}>
                                <RaisedButton
                                    label="История"
                                    primary={true}
                                    style={styleBnt}
                                    labelStyle={labelStyle}
                                />
                            </Link>

                            <Link to={'/settings'}>
                                <RaisedButton
                                    label="Настройки"
                                    primary={true}
                                    style={styleBnt}
                                    labelStyle={labelStyle}
                                />
                            </Link>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MainMenu));

MainMenu.propTypes  = {
    //listData: PropTypes.array.isRequired
};




