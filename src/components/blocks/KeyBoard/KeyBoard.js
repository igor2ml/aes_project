/**
 * Created by i.chernyakov on 12.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import TextField from 'material-ui/TextField';
import Keyboard from 'react-material-ui-keyboard';
import { extendedKeyboard } from 'react-material-ui-keyboard/layouts';

class KeyBoard extends Component {

    handleInput(input) {
        const onChange = this.props.onChange;
        onChange(input);
    }

    handleChangeInput(e) {
        const onChange = this.props.onChange;
        onChange(e.target.value);
    }

    render() {
        const value = this.props.value ? this.props.value : '';
        const hintText = this.props.hintText ? this.props.hintText : '';
        const floatingLabelText = this.props.floatingLabelText ? this.props.floatingLabelText : '';
        const isShowKeyboard = !!this.props.isShowKeyboard;
        const disabled = this.props.disabled;
        const style = this.props.style;
        const floatingLabelStyle = this.props.floatingLabelStyle;
        const fullWidth = this.props.fullWidth;
        return <Keyboard
            textField={<TextField
                value={value}
                onChange={(e)=>{this.handleChangeInput(e)}}
                hintText={hintText}
                floatingLabelText={floatingLabelText}
                disabled={disabled}
                style={{...style}}
                floatingLabelStyle={floatingLabelStyle}
                fullWidth={fullWidth}

            />
            }
            automatic={isShowKeyboard}
            onInput={this.handleInput.bind(this)}
            layouts={[extendedKeyboard]}
        />;
    }
}


function mapStateToProps (state) {
    return{
        isShowKeyboard: state.settings.isShowKeyboard
    }
}


function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KeyBoard);



import PropTypes from 'prop-types';
KeyBoard.propTypes  = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired, //@param value - возращаемое занчение инпута для изменения state родителя
    hintText: PropTypes.string,
    floatingLabelText: PropTypes.string,
    disabled: PropTypes.bool,
    style: PropTypes.object, //стили для материл инпута применяются и для родителя - нужно это учитывать
    floatingLabelStyle: PropTypes.object, //стили для материл инпута
    fullWidth: PropTypes.bool //100% ширины
};