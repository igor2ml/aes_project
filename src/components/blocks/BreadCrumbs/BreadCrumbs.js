/**
 * Created by i.chernyakov on 12.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './BreadCrumbs.sass';
import {Row, Col} from 'react-material-responsive-grid';

class BreadCrumbs extends Component {

    getListBreadCrumb() {
        //получим полный список хлебных крошек
        const fullListBreadCrumb = this.props.list ? this.props.list : [];
        //уберем ссылку у последнего элемента
        const preparedFullListBreadCrumb = fullListBreadCrumb.map((item, idx)=>{
            const IS_LAST_ELEMENT = (fullListBreadCrumb.length-1) === idx;
            if (IS_LAST_ELEMENT) {
                return {title: item.title}
            }
            return item;
        });
        //вернем список в с соответвием с условием "показывать мксимум 2 пункта, для длинных списков"
        return preparedFullListBreadCrumb.length>3 ?
            (()=>{
                const list = [];
                list.push(preparedFullListBreadCrumb[0]);
                list.push({title: '...'});
                list.push(preparedFullListBreadCrumb[preparedFullListBreadCrumb.length-2]);
                list.push(preparedFullListBreadCrumb[preparedFullListBreadCrumb.length-1]);
                return list;
            })() :
            preparedFullListBreadCrumb;
    }

    render() {
        const listBreadCrumb = this.getListBreadCrumb();
        return(
            <div className='wrap-block-BreadCrumb'>
                <Row center={['md', 'lg']}>
                    <Col>
                        <ul className='list-bread-crumb'>

                            {
                                listBreadCrumb.map((item, idx)=>{
                                    const slash = listBreadCrumb.length-1 === idx ? '' : ' - ';
                                    if (item.link) {
                                        return(
                                            <li className='list-bread-crumb__item' key={idx}>
                                                <Link to={item.link} className='list-bread-crumb__link'>
                                                    {item.title} {slash}
                                                </Link>
                                            </li>
                                        )
                                    } else {
                                        return(
                                            <li className='list-bread-crumb__item
                                                    list-bread-crumb__item_text'
                                                key={idx}>

                                                {item.title} {slash}
                                            </li>
                                        )
                                    }

                                })
                            }
                        </ul>
                    </Col>
                </Row>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BreadCrumbs);

BreadCrumbs.propTypes  = {
    list: PropTypes.array.isRequired // [{title: '', link: ''/null}]
};