/**
 * Created by i.chernyakov on 16.11.2017.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import {theme} from '../../../theme';
import './TopBar.sass'
import {BreadCrumbs} from '../../blocks'

import {IconMenu, MenuItem, IconButton, Toggle} from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

import {toggleShowKeyboard} from '../../../actions/toggleShowKeyboard';


class TopBar extends Component {

    componentWillMount(){
        if (window.innerWidth < 1024) {
            this.props.pageActions.toggleShowKeyboard();
        }
    }
    
    onGoToBack(){
        this.props.history.goBack();
    }

    onShowKeyboard(){
        this.props.pageActions.toggleShowKeyboard();
    }
    
    render() {
        const user = this.props.user;
        const isShowKeyboard = !!this.props.isShowKeyboard;
        const breadCrumbs = this.props.breadCrumbs ? this.props.breadCrumbs : [];

        return <div className="wrap-top-bar">
            <div className="top-bar clearfix"
                 style={{
                     backgroundColor: theme.mainColor
                 }}
            >
                {/*<i className="top-bar__back fa fa-reply" aria-hidden="true"*/}
                {/*onClick={this.onGoToBack.bind(this)}*/}
                {/*></i>*/}
                <IconButton
                    className={'top-bar__back'}
                    onClick={this.onGoToBack.bind(this)}
                >
                    <ArrowBack
                        color="#fff"
                    />
                </IconButton>
                <div className="top-bar__bread-crumbs">
                    <BreadCrumbs list={breadCrumbs}/>
                </div>
                <div className="top-bar__menu">
                    <IconMenu
                        iconButtonElement={<IconButton><MoreVertIcon color='#fff'/></IconButton>}
                        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                    >
                        {user.isLogin &&
                        <span>
                            <MenuItem primaryText={user.login} />
                            <MenuItem primaryText="Выйти"
                                      onClick={()=>{console.log('выйти')}}
                            />
                            <MenuItem>
                                <Toggle
                                    label={<i className="fa fa-keyboard-o"
                                              style={{fontSize: 30, lineHeight: '47px'}}
                                    ></i>}
                                    iconStyle={{marginTop: 14}}
                                    toggled={isShowKeyboard}
                                    onToggle={this.onShowKeyboard.bind(this)}
                                />
                            </MenuItem>

                        </span>
                        }
                        {!user.isLogin &&
                        <span>
                            <MenuItem primaryText="Войти"
                                      onClick={()=>{console.log('войти')}}
                            />
                        </span>

                        }


                    </IconMenu>
                </div>

            </div>

        </div>
    }
}


function mapStateToProps (state) {
    return{
        user: state.settings.user,
        isShowKeyboard: state.settings.isShowKeyboard
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            toggleShowKeyboard
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TopBar));

TopBar.propTypes  = {
    //listData: PropTypes.array.isRequired
    breadCrumbs: PropTypes.array
};







