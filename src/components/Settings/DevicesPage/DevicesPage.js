/**
 * Created by i.chernyakov on 14.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {List, ListItem, Divider} from 'material-ui';
import {TopBar} from 'blocks';
import {style} from 'config';

import './DevicesPage.sass';

class DevicesPage extends Component {


    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Подключенные устройства', link: '/device'}];
        const devices = this.props.devices ? this.props.devices : [];
        const firstDevices = devices.slice(0,4);
        const lastDevices = devices.slice(4);
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} xs8={8} sm={6} md={4} mdOffset={2} lg={4} lgOffset={2}>
                            <List>
                                <Divider/>
                                {
                                    firstDevices.map((item, idx)=>{
                                        return <Link key={idx} to={item.link} className="link-wrap">
                                            <ListItem
                                                primaryText={item.name}
                                            />
                                            <Divider/>
                                        </Link>
                                    })
                                }
                            </List>

                        </Col>
                        <Col xs4={4} xs8={8} sm={6} md={4}  lg={4} lgOffset={2}>
                            <List>
                                <Divider/>
                                {
                                    lastDevices.map((item, idx)=>{
                                        return <Link key={idx} to={item.link} className="link-wrap">
                                            <ListItem
                                                primaryText={item.name}
                                            />
                                            <Divider/>
                                        </Link>
                                    })
                                }
                            </List>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        devices: state.devices.devices
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DevicesPage);

DevicesPage.propTypes  = {
    //listData: PropTypes.array.isRequired
    devices: PropTypes.array.isRequired
};

