/**
 * Created by i.chernyakov on 18.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './UserAccount.sass';
import {UserAccountForm} from './UserAccountForm';

export class UserAccount extends Component {

    render() {
        const breadCrumbs = this.props.breadCrumbs ? this.props.breadCrumbs : [];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        // center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col
                            xs4={4} sm={12} md={12} lg={12}
                        >
                            <div className="form-center">

                                <UserAccountForm/>

                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


import PropTypes from 'prop-types';
UserAccount.propTypes  = {
    //listData: PropTypes.array.isRequired
    breadCrumbs: PropTypes.array.isRequired

};
