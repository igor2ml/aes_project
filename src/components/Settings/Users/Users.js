/**
 * Created by i.chernyakov on 14.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {List, ListItem, Avatar, RaisedButton, Paper} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './Users.sass';

import {Link} from 'react-router-dom';

class Accounts extends Component {
    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Учетные записи', link: '/users'}];
        const accounts = this.props.accounts ? this.props.accounts : [];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">
                                <Paper zDepth={2}>
                                    <List>
                                        {
                                            accounts.map((item, idx)=>{
                                                return <div key={idx}>
                                                    <ListItem
                                                        key={idx}
                                                        primaryText={item.name}
                                                        leftAvatar={<Avatar src="https://avatars.mds.yandex.net/get-pdb/1016500/b5f681c9-801e-44b2-add5-d8a61581abdd/s1200" />}

                                                    />
                                                </div>
                                            })
                                        }
                                    </List>
                                </Paper>
                                <br/><br/><br/>
                                <Link to={'/users--new'}>
                                    <RaisedButton
                                        label='Создать'
                                        primary={true}
                                    />
                                </Link>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        accounts: state.settings.accounts
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accounts);

import PropTypes from 'prop-types';
Accounts.propTypes  = {
    accounts: PropTypes.array.isRequired
};









