/**
 * Created by i.chernyakov on 14.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';
import {withRouter} from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {DatePicker, TimePicker, RaisedButton} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './DateTime.sass';

class DateTime extends Component {

    onSave(){
        console.log(this.state);
        toastr.success('Сохранение', 'Параметры сохранены');
    }

    onCancel() {
        this.props.history.goBack();
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Дата-время', link: '/date-time'}];
        const style = {
            btn: {
                margin: 30
            }
        };
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        // center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">
                                <DatePicker
                                    floatingLabelText="Дата"
                                />
                                <TimePicker
                                    format="24hr"
                                    floatingLabelText="Время"
                                />
                                <br/>
                            </div>

                            <Row center={['md', 'lg']}>
                                <Col>
                                    <RaisedButton
                                        label='Отмена'
                                        primary={true}
                                        onClick={this.onCancel.bind(this)}
                                        style={style.btn}
                                    />
                                    <RaisedButton
                                        label='Сохранить'
                                        primary={true}
                                        onClick={this.onSave.bind(this)}
                                        style={style.btn}
                                    />
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        days: state.settings.days,
        months: state.settings.months,
        years: state.settings.years
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DateTime));

DateTime.propTypes  = {
    //listData: PropTypes.array.isRequired
};







