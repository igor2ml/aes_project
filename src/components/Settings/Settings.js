/**
 * Created by i.chernyakov on 17.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton} from 'material-ui';

import {TopBar} from 'blocks';


import './Settings.sass';

class Settings extends Component {

    style = {
        btn: {
            margin: 12,
            width: 200,
            height: 90
        },
        btnLabelStyle: {
            fontSize: 16
        }
    };

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}];
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col>

                            <Link to={'/users'}>
                                <RaisedButton
                                    label={'Пользователи'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                            <Link to={'/date-time'}>
                                <RaisedButton
                                    label={'Дата/время'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                            <Link to={'/device'}>
                                <RaisedButton
                                    label={'Устройства'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                            <br/>
                            <Link to={'glp'}>
                                <RaisedButton
                                    label={'GLP'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                            <Link to={'/'}>
                                <RaisedButton
                                    label={'О приборе'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                            <Link to={'/service'}>
                                <RaisedButton
                                    label={'Сервис'}
                                    primary={true}
                                    style={this.style.btn}
                                    labelStyle={this.style.btnLabelStyle}
                                />
                            </Link>

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);

import PropTypes from 'prop-types';
Settings.propTypes  = {
    //listData: PropTypes.array.isRequired
};