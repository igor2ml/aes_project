/**
 * Created by i.chernyakov on 16.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {List, ListItem} from 'material-ui/List';
import IconAdd from 'material-ui/svg-icons/content/add';
import {addActionToQueue, createQueueMethod} from '../../../../actions/methodsActions';

class AvailableActions extends Component {

    addAction(action) {
        const isHasQueues = Array.isArray(this.props.queues) ? this.props.queues.length>0 : false;
        if (isHasQueues) {
            this.props.pageActions.addActionToQueue(action);
        }
    }

    createQueue() {
        this.props.pageActions.createQueueMethod();
    }
    
    render() {
        const availableActions = Array.isArray(this.props.availableActions) ? this.props.availableActions : [];
        const isHasQueues = Array.isArray(this.props.queues) ? this.props.queues.length>0 : false;
        return <div style={{maxHeight: 'calc(100vh - 60px)', overflowY: 'auto'}}>
            <List>
                <ListItem
                    onClick={this.createQueue.bind(this)}
                    primaryText={'Новая очередь'}
                    rightIcon={<div><IconAdd/></div>}
                />
                {
                    availableActions.map((item, idx)=>{
                        return <ListItem
                            onClick={()=>{this.addAction(item)}}
                            primaryText={item.title}
                            key={idx}
                            rightIcon={
                                <div>
                                    {isHasQueues &&
                                        <IconAdd/>
                                    }
                                </div>
                            }
                        />
                    })

                }
            </List>
        </div>
    }
}

function mapStateToProps (state) {
    return{
        availableActions: state.methods.availableActions,
        queues: state.methods.newMethod.program.queues
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            addActionToQueue,
            createQueueMethod
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AvailableActions);

//import PropTypes from 'prop-types';
//AvailableActions.propTypes  = {
    //listData: PropTypes.array.isRequired
//};
