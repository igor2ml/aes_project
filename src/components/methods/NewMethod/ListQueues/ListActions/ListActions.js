/**
 * Created by i.chernyakov on 16.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    SortableContainer,
    SortableElement,
    SortableHandle,
    arrayMove,
} from 'react-sortable-hoc';
import {List, ListItem} from 'material-ui/List'
import IconDragHandle from 'material-ui/svg-icons/editor/drag-handle';
import IconDelete from 'material-ui/svg-icons/action/delete';
import {changeOrderActions, deleteActionFromQueue} from '../../../../../actions/methodsActions';


const DragHandle = SortableHandle(() => <IconDragHandle style={{cursor: 'pointer', position: 'absolute', left: 4, margin: 12, top: 0}}/>); // This can be any component you want

class ActionItem extends Component {
    onClickDelete() {
        const item = this.props.item;
        const queuesArrayId = this.props.queuesArrayId;
        this.props.pageActions.deleteActionFromQueue(queuesArrayId, item);
    }

    render() {
        const item = this.props.item;
        return <ListItem
            primaryText={item.title}
            leftIcon={<DragHandle/>}
            rightIcon={<div><IconDelete onClick={this.onClickDelete.bind(this)}/></div>}
            style={{position: 'relative'}}
        />
    }
}
const ConnectActionItem = connect(()=>{return{}},(dispatch)=>{return {
    pageActions: bindActionCreators({
        deleteActionFromQueue
    }, dispatch)
}})(ActionItem);

const SortableItem = SortableElement(({item, queuesArrayId}) => {
    return <ConnectActionItem item={item} queuesArrayId={queuesArrayId}/>
});

const SortableList = SortableContainer(({items, queuesArrayId}) => {
    return <List>
        {items.map((value, index) => {
            return <SortableItem key={`item-${index}`} index={index} item={value} queuesArrayId={queuesArrayId}/>
        })}
    </List>;

});

class ListActions extends Component {
    onSortEnd = ({oldIndex, newIndex}) => {
        const actions = this.props.actions;
        const newActions = arrayMove(actions, oldIndex, newIndex);
        const queuesArrayId = this.props.queuesArrayId;
        this.props.pageActions.changeOrderActions(newActions,queuesArrayId)
    };

    render() {
        const actions = Array.isArray(this.props.actions) ? this.props.actions : [];
        const queuesArrayId = this.props.queuesArrayId;
        return <SortableList items={actions} queuesArrayId={queuesArrayId} onSortEnd={this.onSortEnd} useDragHandle={true} />;
    }
}

function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            changeOrderActions
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListActions);

import PropTypes from 'prop-types';
ListActions.propTypes  = {
    actions: PropTypes.array.isRequired, //список добавленных действий
    queuesArrayId: PropTypes.number.isRequired //индекс массива очереди внутри который распологаются данные действия
};