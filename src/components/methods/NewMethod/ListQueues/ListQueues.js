/**
 * Created by i.chernyakov on 16.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Row, Col} from 'react-material-responsive-grid';
import {
    SortableContainer,
    SortableElement,
    SortableHandle,
    arrayMove,
} from 'react-sortable-hoc';
import {Paper, IconButton, FontIcon } from 'material-ui';
import {CardHeader} from 'material-ui/Card';
import IconCheck from 'material-ui/svg-icons/navigation/check';
import IconDelete from 'material-ui/svg-icons/action/delete';
import {changeOrderQueues, doActiveQueue, deleteQueueMethod} from '../../../../actions/methodsActions';
import ListActions from './ListActions/ListActions';
import {theme} from '../../../../theme';

const DragHandle = SortableHandle(({item}) => {
    return <div style={{cursor: 'move'}}>
        <CardHeader
            title={item.title}
        />
    </div>
});

class QueueItem extends Component {
    onClickActive() {
        const queuesArrayId = this.props.item.queuesArrayId;
        this.props.pageActions.doActiveQueue(queuesArrayId);
    }

    onClickDelete() {
        const queuesArrayId = this.props.item.queuesArrayId;
        this.props.pageActions.deleteQueueMethod(queuesArrayId);
    }

    render() {
        const item = this.props.item;
        const isActive = item.isactive;
        return <Paper zDepth={isActive ? 4 : 1}>
            <Row between={['md', 'lg']} className={'card-element-sides'}>
                <Col>
                    <DragHandle item={item}/>
                </Col>
                <Col>
                    <IconButton
                        onClick={this.onClickActive.bind(this)}
                    >
                        <IconCheck color={isActive ? theme.mainColor : theme.disabledColor}/>
                    </IconButton>
                    <IconButton
                        onClick={this.onClickDelete.bind(this)}
                    >
                        <IconDelete/>
                    </IconButton>
                </Col>
            </Row>
            <ListActions
                actions={item.actions}
                queuesArrayId={item.queuesArrayId}
            />
        </Paper>
    }
}
const ConnectQueueItem = connect(()=>{return{}},(dispatch)=>{return {
    pageActions: bindActionCreators({
        doActiveQueue,
        deleteQueueMethod
    }, dispatch)
}})(QueueItem);

const SortableItem = SortableElement(({item}) => {
    return (
        <div style={{marginBottom: 15}}>
            <ConnectQueueItem item={item}/>
        </div>
    );
});

const SortableList = SortableContainer(({items}) => {
    return <div>
        {items.map((value, index) => (
            <SortableItem
                key={`item-${index}`} 
                index={index} 
                item={{...value, queuesArrayId: index}}
            />
        ))}
    </div>;
});

class ListQueues extends Component {
    onSortEnd = ({oldIndex, newIndex}) => {
        const queues = this.props.queues;
        const newQueues = arrayMove(queues, oldIndex, newIndex);
        this.props.pageActions.changeOrderQueues(newQueues);
    };
    render() {
        const queues = this.props.queues;
        return <SortableList items={queues} onSortEnd={this.onSortEnd} useDragHandle={true}/>;
    }
}

function mapStateToProps (state) {
    return{
        queues: state.methods.newMethod.program.queues
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            changeOrderQueues
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListQueues);
