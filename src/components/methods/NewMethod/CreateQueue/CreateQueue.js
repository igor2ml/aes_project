/**
 * Created by i.chernyakov on 16.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createQueueMethod} from '../../../../actions/methodsActions';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconContentAdd from 'material-ui/svg-icons/content/add';

class CreateQueue extends Component {

    createQueue() {
        this.props.pageActions.createQueueMethod();
    }

    render() {
        return <div>
            <FloatingActionButton
                onClick={this.createQueue.bind(this)}
            >
                <IconContentAdd />
            </FloatingActionButton>
        </div>
    }
}

function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({
            createQueueMethod
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateQueue);

//import PropTypes from 'prop-types';
//CreateQueue.propTypes  = {
    //listData: PropTypes.array.isRequired
//};
