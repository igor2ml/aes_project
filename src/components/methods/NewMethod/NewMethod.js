/**
 * Created by i.chernyakov on 16.04.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton} from 'material-ui';
import {TopBar} from 'blocks';
import {style} from 'config';
import AvailableActions from './AvailableActions/AvailableActions';
import ProgramsMethod from './ListQueues/ListQueues';
//import CreateQueue from './CreateQueue/CreateQueue';
import Sticky from 'react-sticky-el';

class NewMethod extends Component {
    constructor(props){
        super(props);
        this.state = {
            isFixedAvailableActions: false
        }
    }

    onFixedAvailableActions() {
        this.setState({isFixedAvailableActions: !this.state.isFixedAvailableActions});
    }

    render() {
        const breadCrumbs = [{title: 'Главное меню'}];
        const isHasQueues = Array.isArray(this.props.queues) ? this.props.queues.length>0 : false;
        const isFixedAvailableActions = this.state.isFixedAvailableActions;
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        //middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={4} lg={4}>
                            <Sticky onFixedToggle={this.onFixedAvailableActions.bind(this)}>
                                <div style={{
                                    paddingTop: isFixedAvailableActions ? 50 : 0,
                                    transition: 'padding-top 0.25s ease-out'
                                }}>
                                    <AvailableActions/>
                                </div>
                            </Sticky>
                        </Col>
                        <Col xs4={4} sm={12} md={8} lg={8}>
                            <div style={{padding: 10}}>
                                <ProgramsMethod/>
                                <br/>
                                {isHasQueues &&
                                <Row center={['sm', 'md', 'lg']}>
                                    <RaisedButton
                                        primary={true}
                                        label={'Сохранить'}
                                    />
                                </Row>
                                }
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        queues: state.methods.newMethod.program.queues
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewMethod);

import PropTypes from 'prop-types';
NewMethod.propTypes  = {
    //listData: PropTypes.array.isRequired
};
