/**
 * Created by i.chernyakov on 15.02.2018.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {RaisedButton, SelectField, MenuItem} from 'material-ui';
import {CardHeader} from 'material-ui/Card';
import {TopBar} from 'blocks';
import {style} from 'config';


class Service extends Component {

    constructor(props){
        super(props);
        this.state = {
            pumps_count: 3,
            burette_count: 3
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    getBurettes(){
        const buretteCount = this.state.burette_count;
        const burettes = [];
        for (let i=0; i< buretteCount; i++) {
            burettes.push({title: `Бюретка ${i+1}`,value: 5});
        }
        return burettes;
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}, {title: 'Сервис'}];
        const burettes = this.getBurettes();
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <div className="form-center">
                                <CardHeader
                                    title={'Комплектация прибора'}
                                    style={{padding: 0}}
                                />
                                <SelectField
                                    floatingLabelText="Количество насосов"
                                    value={this.state.pumps_count}
                                    onChange={(a,b,value)=>{this.onChangeInput('pumps_count', value)}}
                                >
                                    <MenuItem value={1} primaryText="1" />
                                    <MenuItem value={2} primaryText="2" />
                                    <MenuItem value={3} primaryText="3" />
                                    <MenuItem value={4} primaryText="4" />
                                </SelectField>

                                <SelectField
                                    floatingLabelText="Количество бюреток"
                                    value={this.state.burette_count}
                                    onChange={(a,b,value)=>{this.onChangeInput('burette_count', value)}}
                                >
                                    <MenuItem value={1} primaryText="1" />
                                    <MenuItem value={2} primaryText="2" />
                                    <MenuItem value={3} primaryText="3" />
                                </SelectField>

                                <div style={{marginTop: 30}}>
                                    <CardHeader
                                        title={'Конфигурация бюреток'}
                                        style={{padding: 0}}
                                    />
                                    {
                                        burettes.map((item, idx)=>{
                                            return <SelectField
                                                key={idx}
                                                floatingLabelText={item.title}
                                                value={item.value}
                                            >
                                                <MenuItem value={1} primaryText="1" />
                                                <MenuItem value={5} primaryText="5" />
                                                <MenuItem value={10} primaryText="10" />
                                            </SelectField>
                                        })
                                    }
                                </div>

                                <br/>
                                <RaisedButton
                                    primary={true}
                                    label={'Сохранить'}
                                />
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Service);

import PropTypes from 'prop-types';
Service.propTypes  = {
    //listData: PropTypes.array.isRequired
};
