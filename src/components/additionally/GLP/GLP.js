/**
 * Created by i.chernyakov on 18.12.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import {Toggle, SelectField, MenuItem} from 'material-ui';

import {TopBar} from 'blocks';
import {style} from 'config';

import './GLP.sass';

class GLP extends Component {

    constructor(props){
        super(props);
        this.state = {
            glp: false,
            period: 1
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    onChangeCheckbox(e, name) {
        const data = {};
        data[name] = !this.state[name];
        this.setState(data);
    }

    render() {
        const breadCrumbs = [{title: 'Настройки', link: '/settings'}];
        const periods = [];
        for (let i=1; i<=90; i++) {
            periods.push(i)
        }
        console.log(periods);
        return(
            <div>
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        //center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col xs4={4} xs8={4} sm={6} md={6} lg={6}>
                            <div className='form-center'>
                                <Toggle
                                    label="режим GLP"
                                    labelPosition="right"
                                    toggled={this.state.glp}
                                    onToggle={(e)=>{this.onChangeCheckbox(e, 'glp')}}
                                />
                            </div>

                        </Col>
                        <Col xs4={4} xs8={4} sm={6} md={6} lg={6}>
                            <SelectField
                                floatingLabelText="Периодичность калибровки, дни"
                                value={this.state.period}
                                disabled={!this.state.glp}
                                onChange={(a,b, value)=>{this.onChangeInput('period', value)}}
                            >
                                {periods.map((item)=>{
                                    return <MenuItem key={item} value={item} primaryText={`${item}`} />
                                })}

                            </SelectField>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GLP);

import PropTypes from 'prop-types';
GLP.propTypes  = {
    //listData: PropTypes.array.isRequired
};
