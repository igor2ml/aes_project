/**
 * Created by i.chernyakov on 17.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import {TextField, SelectField, MenuItem, RaisedButton} from 'material-ui';

class AuthorizationForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            role: 1
        }
    }

    handSelectField(event, index, value, name){
        const data = {};
        data[name] = value;
        this.setState(data);
    }
    
    render() {
        return(
            <div className="authorization-form form-center">

                <TextField
                    hintText="Логин"
                    floatingLabelText="Логин"
                /><br />
                <TextField
                    hintText="Пароль"
                    floatingLabelText="Пароль"
                /><br />

                <SelectField
                    floatingLabelText="Frequency"
                    value={this.state.role}
                    onChange={(a,b,c)=>{this.handSelectField(a,b,c, 'role')}}
                >
                    <MenuItem value={1} primaryText="Администратор" />
                    <MenuItem value={2} primaryText="Пользователь " />
                    <MenuItem value={3} primaryText="Ученик " />
                </SelectField>
                <br />
                <br />
                <RaisedButton
                    label="Войти"
                    primary={true}
                />
                <br/>
                <br/>
                <Link to={'/register'}>
                    <RaisedButton
                        label="Создать аккаунт"
                    />
                </Link>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationForm);

AuthorizationForm.propTypes  = {
    //listData: PropTypes.array.isRequired
};
