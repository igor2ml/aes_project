/**
 * Created by i.chernyakov on 14.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import PropTypes from 'prop-types';

import {TopBar} from '../blocks/index';
import {Grid, Row, Col} from 'react-material-responsive-grid';

import './Authorization.sass';
import AuthorizationForm from './AuthorizationForm';

class Authorization extends Component {

    render() {
        const breadCrumbs = [{title: 'Авторизация'}];
        return(
            <div className="authorization">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col lg={12} md={12} sm={12} xs4={4}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        className={'content'}
                        middle={['lg', 'md', 'sm', 'xs4']}
                    >
                        <Col lg={12} md={12} sm={12} xs4={4}>
                            <AuthorizationForm/>
                        </Col>
                    </Row>


                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);

Authorization.propTypes  = {
    //listData: PropTypes.array.isRequired
};

