/**
 * Created by i.chernyakov on 12.11.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import {toastr} from 'react-redux-toastr';
import {withRouter} from 'react-router-dom';
import {TopBar} from '../../blocks/index';
import KeyBoard from '../../blocks/KeyBoard/KeyBoard';

import './NewAccount.sass';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';

class NewAccount extends Component {

    constructor(props){
        super(props);
        this.state = {
            login: '',
            password: '',
            role: '',
            isOneUser: true,
            isLoginAuto: false
        }
    }

    onChangeInput(name, value) {
        const data = {};
        data[name] = value;
        this.setState(data);
    }

    onSave(){
        console.log(this.state);
        toastr.success('Сохранение', 'Параметры сохранены');
    }

    onCancel() {
        this.props.history.goBack();
    }

    render() {
        const breadCrumbs = this.props.breadCrumbs ? this.props.breadCrumbs : [];
        const style = {
            checkbox: {
                marginTop: 15
            },
            btn: {
                margin: 30
            }
        };
        return(
            <div className="new-account">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} xs8={8} sm={12} md12={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>

                    <Row
                        // center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'new-account__row'}
                    >
                        <Col xs4={4} xs8={8} sm={12} md12={12} lg={12}>
                            <div className="new-account__content">
                                <KeyBoard
                                    value={this.state.login}
                                    hintText="Логин"
                                    floatingLabelText="Логин"
                                    onChange={(value)=>{this.onChangeInput('login', value)}}
                                />
                                <KeyBoard
                                    value={this.state.password}
                                    hintText="Пароль"
                                    floatingLabelText="Пароль"
                                    onChange={(value)=>{this.onChangeInput('password', value)}}
                                />


                                <SelectFieldExampleSimple/>

                                <Checkbox
                                    label='Единственный пользователь'
                                    style={style.checkbox}
                                />
                                <Checkbox
                                    label='Входить автоматически'
                                    style={style.checkbox}
                                    defaultChecked={true}
                                />
                                <br/>
                            </div>
                            <Row center={['md', 'lg']}>
                                <Col>
                                    <RaisedButton
                                        label='Отмена'
                                        primary={true}
                                        onClick={this.onCancel.bind(this)}
                                        style={style.btn}
                                    />
                                    <RaisedButton
                                        label='Сохранить'
                                        primary={true}
                                        onClick={this.onSave.bind(this)}
                                        style={style.btn}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

function mapStateToProps () {
    return{

    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NewAccount));

NewAccount.propTypes  = {
    breadCrumbs: PropTypes.array.isRequired
};


class SelectFieldExampleSimple extends Component {
    state = {
        value: 1,
    };

    handleChange = (event, index, value) => this.setState({value});

    render() {
        return (
            <div>
                <SelectField
                    floatingLabelText="Роль"
                    value={this.state.value}
                    onChange={this.handleChange}
                >
                    <MenuItem value={1} primaryText="Администратор" />
                    <MenuItem value={2} primaryText="Пользователь" />
                    <MenuItem value={3} primaryText="Ученик" />
                </SelectField>
                <br />

            </div>
        );
    }
}

