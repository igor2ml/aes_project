/**
 * Created by i.chernyakov on 12.11.2017.
 */
//выбор языка
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';

import './Language.sass';

import { Grid, Row, Col } from 'react-material-responsive-grid'
import RaisedButton from 'material-ui/RaisedButton';

import {TopBar} from '../blocks/index';

class Language extends Component {

    onClick(){
        console.log('click');
    }

    render() {
        const style = {
            margin: 12,
        };
        const breadCrumbs = [{title: 'Выбор языка', link: '/language'}];
        return(
            <div className="start-screen">
                <Grid
                    fixed={'center'}
                >
                    <Row>
                        <Col xs4={4} sm={12} md={12} lg={12}>
                            <TopBar
                                breadCrumbs={breadCrumbs}
                            />
                        </Col>
                    </Row>
                    <Row
                        center={['xs4', 'sm', 'md', 'ld']}
                        middle={['xs4', 'sm', 'md', 'ld']}
                        className={'content'}
                    >
                        <Col>

                            <h1>Выберите язык</h1>
                            <RaisedButton
                                label="Ru"
                                onClick={this.onClick.bind(this)}
                                style={style}
                                primary={true}
                            />
                            <RaisedButton
                                label="En"
                                primary={true}
                                style={style}
                            />

                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return{
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators({

        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Language);

Language.propTypes  = {
    //listData: PropTypes.array.isRequired
};
