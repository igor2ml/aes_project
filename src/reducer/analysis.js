/**
 * Created by i.chernyakov on 11.05.2018.
 */
const initState = {
    program: {
        queues: [ //очереди
            {
                title: 'Программа метода',
                isactive: true,
                activesamples: 1,
                samples:[1,2,3,4,5,6,7,8,9,10],
                actions:[
                    {id: 3, title: 'Промывка'},
                    {id: 4, title: 'Титрование'},
                    {id: 5, title: 'Калибровка pH электрода'}
                ]
            },
            {
                title: 'Программа метода',
                isactive: false,
                activesamples: null,
                samples:[1,2,3,4,5,6,7,8,9,10],
                actions:[
                    {id: 9, title: 'Добавить реагент'},
                    {id: 10, title: 'Перемешивание'},
                    {id: 11, title: 'Нагревание образца'}
                ]
            },
            {
                title: 'Программа метода',
                isactive: false,
                activesamples: null,
                samples:[1,2,3,4,5,6,7,8,9,10],
                actions:[
                    {id: 13, title: 'Титрование'},
                    {id: 14, title: 'Калибровка pH электрода'},
                    {id: 15, title: 'pH-метр'},
                ]
            },
            {
                title: 'Программа метода',
                isactive: false,
                activesamples: null,
                samples:[1,2,3,4,5,6,7,8,9,10],
                actions:[
                    {id: 6, title: 'pH-метр'},
                    {id: 7, title: 'Довести до pH/мВ'},
                    {id: 8, title: 'Определение титра'}
                ]
            }
        ]
    }
};

export default function analysis(state = initState, action) {
    switch(action.type) {

        default:
            return state;
    }
};
