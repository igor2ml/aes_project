/**
 * Created by i.chernyakov on 16.04.2018.
 */
import {
    createQueueMethod,
    deleteQueueMethod,
    changeOrderActions,
    doActiveQueue,
    addActionToQueue,
    deleteActionFromQueue
} from './helpers/methodsHelpers';

const initState = {
    availableActions: [
        // {id: 1, title: 'Новая очередь'},
        // {id: 2, title: 'Конец очереди'},
        {id: 3, title: 'Промывка'},
        {id: 4, title: 'Титрование'},
        {id: 5, title: 'Калибровка pH электрода'},
        {id: 6, title: 'pH-метр'},
        {id: 7, title: 'Довести до pH/мВ'},
        {id: 8, title: 'Определение титра'},
        {id: 9, title: 'Добавить реагент'},
        {id: 10, title: 'Перемешивание'},
        {id: 11, title: 'Нагревание образца'},
        {id: 12, title: 'Пауза'},
        {id: 13, title: 'Титрование'},
        {id: 14, title: 'Калибровка pH электрода'},
        {id: 15, title: 'pH-метр'},
        {id: 16, title: 'Довести до pH/мВ'},
        {id: 17, title: 'Определение титра'},
        {id: 18, title: 'Добавить реагент'},
        {id: 19, title: 'Перемешивание'}
    ],
    newMethod: {
        program: {
            queues: [ //очереди
                //{title: 'Программа метода', isactive, samples:[], actions:[]} //действия
            ]
        }
    }
};

export default function methods(state = initState, action) {
    switch(action.type) {
        case 'CREATE_QUEUE_METHOD':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: createQueueMethod(state.newMethod.program.queues)
                    }
                }
            };
        case 'DELETE_QUEUE_METHOD':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: deleteQueueMethod(state.newMethod.program.queues, action.payload.queuesArrayId)
                    }
                }
            };
        case 'CHANGE_ORDER_QUEUES':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: action.payload.data
                    }
                }
            };
        case 'CHANGE_ORDER_ACTIONS':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: changeOrderActions(state.newMethod.program.queues, action.payload.data, action.payload.queuesArrayId)
                    }
                }
            };
        case 'DO_ACTIVE_QUEUE':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: doActiveQueue(state.newMethod.program.queues, action.payload.queuesArrayId)
                    }
                }
            };
        case 'ADD_ACTIONTOQUEUE':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: addActionToQueue(state.newMethod.program.queues, action.payload.data)
                    }
                }
            };
        case 'DELETE_ACTIONFROMQUEUE':
            return {
                ...state,
                newMethod: {
                    ...state.newMethod,
                    program: {
                        ...state.newMethod.program,
                        queues: deleteActionFromQueue(state.newMethod.program.queues, action.payload.queuesArrayId, action.payload.action)
                    }
                }
            };
        default:
            return state;
    }
};
