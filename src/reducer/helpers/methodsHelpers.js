/**
 * Created by i.chernyakov on 16.04.2018.
 */

export function createQueueMethod(queues) {
    const notActiveQueues = queues.map((item)=>{return {...item, isactive: false}});
    return notActiveQueues.concat({
        title: `Новая очередь`,
        isactive: true,
        samples:[],
        actions:[]
    });
}

export function deleteQueueMethod(queues = [], queuesArrayId) {
    return queues.filter((item, idx)=>{
        if (idx!==queuesArrayId) {
            return item;
        }
    })
}

export function changeOrderActions(queues, newActions, queuesArrayId) {
    //создать новый список очередей зименив в старом список действий у текущей очереди
    const newQueues = queues.map((item, idx)=>{
        //если это текущая очередь
        if (idx===queuesArrayId) {
            //заменить дейвия и вернуть результат
            return {
                ...item,
                actions: newActions
            }
        } else { //елси нет
            //вернуть то же что и было
            return item;
        }
    });
    //вернуть новый список очередей
    return newQueues
}

export function doActiveQueue(queues,  queuesArrayId) {
    const newQueues = queues.map((item,idx)=>{
        if (idx===queuesArrayId) {
            return {
                ...item,
                isactive: true
            }
        } else {
            return {
                ...item,
                isactive: false
            }
        }
    });
    return newQueues;
}

export function addActionToQueue(queues, action) {
    return queues.map((item)=>{
        if (item.isactive) {
            return {
                ...item,
                actions: item.actions.concat(action)
            }
        } else {
            return item;
        }
    });
}

export function deleteActionFromQueue(queues, queuesArrayId, action) {
    return queues.map((item, idx)=>{
        if (queuesArrayId===idx) {
            return {
                ...item,
                actions: item.actions.filter(i=>i.id!==action.id)
            }
        } else {
            return item;
        }
    });
}