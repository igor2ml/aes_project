/**
 * Created by i.chernyakov on 23.10.2017.
 */

import { combineReducers} from 'redux';

import settings from './settings';
import devices from './devices';
import methods from './methods';
import analyses from './analysis';

import {reducer as toastrReducer} from 'react-redux-toastr'

export default combineReducers({
    settings,
    devices,
    methods,
    analyses,
    toastr: toastrReducer
})


