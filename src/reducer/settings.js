/**
 * Created by i.chernyakov on 12.11.2017.
 */

const initState = {
    user: {
        isLogin: true,
        login: 'LoginUser'
    },
    date: {

    },
    time: {
        hours: '08',
        minutes: '30'
    },
    accounts: [
        {name: 'user'},
        {name: 'new_user user_login'},
        {name: 'user_login'}
    ],
    roles: [
        {id: '1', name: 'Администратор'},
        {id: '2', name: 'Пользователь'},
        {id: '3', name: 'Ученик'}
    ],

    isShowKeyboard: false
};

export default function settings(state = initState, action) {
    switch(action.type) {
        case 'TOGGLE_SHOW-KEYBOARD':
            return {
                ...state,
                isShowKeyboard: !state.isShowKeyboard
            };
        default:
            return state;
    }
};
