/**
 * Created by i.chernyakov on 16.12.2017.
 */
const initState = {
    devices: [
        {id: '2', name: 'рН-блок', link: '/device--ph-block'},
        {id: '3', name: 'Фото-блок', link: '/device--photo-block'},
        {id: '4', name: 'Бюретка', link: '/device--burette'},
        {id: '5', name: 'Перистальтический насос', link: '/device--pump'},
        {id: '1', name: 'Автосамплер', link: '/device--autosampler'},
        {id: '7', name: 'Магнитная мешалка', link: '/device--magnetic-stirrer'},
        {id: '6', name: 'Нагревательный элемент', link: '/device--heat-element'},
        {id: '8', name: 'Термодатчики', link: '/device--termo-sensor'}
        //{id: '8', name: 'Весы', link: '/device--libra'}
    ],
    autosampler: {
        position: 0,
        isready: true,
        speed: 0,
        isliftup: false,
        liftPosition: 0,
        isChangingPosition: false //true - позиция изменяется в данный момент
    },
    ph: {
        electrode: {
            value: 3.05
        },
        thermometer: {
            value: 40.7
        }
    },
    photoblock: {
        red: 10,
        green: 15,
        blue: 20
    }
};

export default function devices(state = initState, action) {
    switch(action.type) {
        case 'GET_STATUS_AUTOSAMPLE_SUCCESS':
            return {
                ...state,
                autosampler: {
                    ...state.autosampler,
                    ...action.payload.data
                }
            };
        case 'SET_SPEED_STIRRER_SUCCESS':
            return {
                ...state,
                autosampler: {
                    ...state.autosampler,
                    speed: action.payload.data
                }
            };
        case 'SET_POSITION_AUTOSAMPLE':
            return {
                ...state,
                autosampler: {
                    ...state.autosampler,
                    isChangingPosition: true
                }
            };
        case 'SET_POSITION_AUTOSAMPLE_SUCCESS':
            return {
                ...state,
                autosampler: {
                    ...state.autosampler,
                    position: action.payload.data,
                    isChangingPosition: false
                }
            };
        case 'SET_POSITION_LIFT_SUCCESS':
            return {
                ...state,
                autosampler: {
                    ...state.autosampler,
                    liftPosition: action.payload.data
                }
            };

        case 'GET_VALUE_ELECTRODE_SUCCESS':
            return {
                ...state,
                ph: {
                    ...state.ph,
                    electrode: {
                        ...state.ph.electrode,
                        value: action.payload.data
                    }
                }
            };
        case 'GET_VALUE_THERMOMETER_SUCCESS':
            return {
                ...state,
                ph: {
                    ...state.ph,
                    thermometer: {
                        ...state.ph.thermometer,
                        value: action.payload.data
                    }
                }
            };

        case 'GET_VALUE_PHOTOBLOCK_SUCCESS':
            return {
                ...state,
                photoblock: {
                    ...state.photoblock,
                    ...action.payload.data
                }
            };

        default:
            return state;
    }
};