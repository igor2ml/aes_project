/**
 * Created by root on 25.02.2018.
 */
import React from 'react';
import {App} from './router';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducer/index';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import './style/common.sass';
import './style/libs/font-awesome-4.7.0/scss/font-awesome.scss';
import 'react-redux-toastr/src/styles/index.scss';

import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
injectTapEventPlugin();

import ReduxToastr from 'react-redux-toastr'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {theme} from './theme';


const store = createStore(rootReducer,
    composeWithDevTools(
        applyMiddleware(
            thunk
            // loggerMiddleware
        )
    )
);


const muiTheme = getMuiTheme({
    fontFamily: theme.fontFamily,
    palette: {
        primary1Color: theme.mainColor,
        primary2Color: theme.mainColor,
        primary3Color: theme.mainColor,
        textColor: theme.mainColor,
        pickerHeaderColor: theme.mainColor,
        shadowColor: theme.mainColor,


        accent1Color: theme.accentColor,
        accent2Color: theme.accentColor,
        accent3Color: theme.accentColor


    },
    button: {
        ...theme.button
    }
});

export default class Core extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div
                        style={{
                            fontFamily: theme.fontFamily,
                            color: theme.mainColor
                        }}
                    >
                        <App/>
                        <ReduxToastr
                            timeOut={4000}
                            newestOnTop={false}
                            preventDuplicates
                            position="bottom-right"
                            transitionIn="fadeIn"
                            transitionOut="fadeOut"
                            progressBar/>
                    </div>
                </MuiThemeProvider>
            </Provider>
        );
    }
};