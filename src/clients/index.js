import directAxios from './directAxios';

const clients = {
  daxios: directAxios
};

export default clients;

