/**
 * Created by Stainwoortsel on 04.03.2017.
 */
import {appConfig} from '../config'
import axios from 'axios'


const directAxios = axios.create({
    baseURL: appConfig.api.base,
    responseType: appConfig.api.type
});

export default directAxios;