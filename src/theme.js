/**
 * Created by i.chernyakov on 16.05.2018.
 */

const theme = {
    fontFamily: 'Roboto, sans-serif', //'Roboto, sans-serif',
    mainColor: '#004D57',//'#004D57'
    button: {
        minWidth: 200,
        height: 48
    },
    accentColor: '#00919c',

    disabledColor: '#E5E5E5'
};

export {
    theme
}

