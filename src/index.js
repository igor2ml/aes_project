/**
 * Created by i.chernyakov on 10.10.2017.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Core from './core';

<Core/>

ReactDOM.render(
    <Core />,
    document.getElementById('root')
);




