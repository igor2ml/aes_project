/**
 * Created by i.chernyakov on 08.02.2018.
 */
import daxios from '../clients/directAxios'
import {toastr} from 'react-redux-toastr';

export function getValuePhotoblock(channel) {
    return function (dispatch) {
        dispatch({
            type: 'GET_VALUE_PHOTOBLOCK',
            payload: {}
        });
        return daxios
            .get('/photo/channel', {
                channel: channel
            })
            .then((res) => {
                console.log(res);
                const data = ()=>{
                    switch (channel){
                        case 0:
                            return {red: res.data.value};
                        case 1:
                            return {green: res.data.value};
                        case 2:
                            return {blue: res.data.value};
                    }
                };
                dispatch({
                    type: 'GET_VALUE_PHOTOBLOCK_SUCCESS',
                    payload: {
                        data: data()
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                dispatch({
                    type: 'GET_VALUE_PHOTOBLOCK_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}
