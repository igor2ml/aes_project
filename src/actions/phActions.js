/**
 * Created by i.chernyakov on 08.02.2018.
 */
import daxios from '../clients/directAxios'

export function getValueElectrode () {
    return function (dispatch) {
        dispatch({
            type: 'GET_VALUE_ELECTRODE',
            payload: {}
        });
        return daxios
            .get('/ph/electrode')
            .then((res) => {
                console.log(res);
                dispatch({
                    type: 'GET_VALUE_ELECTRODE_SUCCESS',
                    payload: {
                        data: res.data.value
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                dispatch({
                    type: 'GET_VALUE_ELECTRODE_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function getValueThermometer () {
    return function (dispatch) {
        dispatch({
            type: 'GET_VALUE_THERMOMETER',
            payload: {}
        });
        return daxios
            .get('/ph/thermometer')
            .then((res) => {
                console.log(res);
                dispatch({
                    type: 'GET_VALUE_THERMOMETER_SUCCESS',
                    payload: {
                        data: res.data.value
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                dispatch({
                    type: 'GET_VALUE_THERMOMETER_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}