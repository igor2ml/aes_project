/**
 * Created by i.chernyakov on 16.12.2017.
 */
import daxios from '../clients/directAxios'
import {toastr} from 'react-redux-toastr';

export function getStatusAutosample() {
    return function (dispatch) {
        dispatch({
            type: 'GET_STATUS_AUTOSAMPLE',
            payload: {}
        });
        return daxios
            .get('/autosample/status')
            .then((res) => {
                console.log(res);
                dispatch({
                    type: 'GET_STATUS_AUTOSAMPLE_SUCCESS',
                    payload: {
                        data: res.data
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                dispatch({
                    type: 'GET_STATUS_AUTOSAMPLE_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setSpeedStirrer(speed) {
    return function (dispatch) {
        dispatch({
            type: 'SET_SPEED_STIRRER',
            payload: {}
        });
        return daxios
            .post('/autosample/speed', {
                speed: speed
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Скорость изменина', 'Установленна скорость ' + speed);
                    dispatch({
                        type: 'SET_SPEED_STIRRER_SUCCESS',
                        payload: {
                            data: speed
                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Изменить скорость не удалось');
                    dispatch({
                        type: 'SET_SPEED_STIRRER_FAIL',
                        payload: {

                        }
                    });
                }

            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Изменить скорость не удалось');
                dispatch({
                    type: 'SET_SPEED_STIRRER_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setPositionAutosample(position) {
    return function (dispatch) {
        dispatch({
            type: 'SET_POSITION_AUTOSAMPLE',
            payload: {}
        });
        return daxios
            .post('/autosample/position', {
                position: position
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Позиция изменина', 'Установленна позиция ' + position);
                    dispatch({
                        type: 'SET_POSITION_AUTOSAMPLE_SUCCESS',
                        payload: {
                            data: position
                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Изменить позицию не удалось');
                    dispatch({
                        type: 'SET_POSITION_AUTOSAMPLE_FAIL',
                        payload: {

                        }
                    });
                }

            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Изменить позицию не удалось');
                dispatch({
                    type: 'SET_POSITION_AUTOSAMPLE_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setPositionLift(position) {
    return function (dispatch) {
        dispatch({
            type: 'SET_POSITION_LIFT',
            payload: {}
        });
        return daxios
            .post('/autosample/lift', {
                position: position
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Позиция изменина', 'Позиция лифта - ' + position===0 ? 'Вверх' : 'Вниз');
                    dispatch({
                        type: 'SET_POSITION_LIFT_SUCCESS',
                        payload: {
                            data: position
                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Изменить позицию лифта не удалось');
                    dispatch({
                        type: 'SET_POSITION_LIFT_FAIL',
                        payload: {

                        }
                    });
                }

            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Изменить позицию лифта не удалось');
                dispatch({
                    type: 'SET_POSITION_LIFT_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}


