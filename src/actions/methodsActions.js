/**
 * Created by i.chernyakov on 16.04.2018.
 */

//добвить очередь в метод
export function createQueueMethod() {
    return {
        type: 'CREATE_QUEUE_METHOD',
        payload: {}
    }
}

//удалить очередь из  метода
export function deleteQueueMethod(queuesArrayId) {
    return {
        type: 'DELETE_QUEUE_METHOD',
        payload: {
            queuesArrayId
        }
    }
}

//изменить порядок программ в методе
export function changeOrderQueues(queues) {
    return {
        type: 'CHANGE_ORDER_QUEUES',
        payload: {
            data: queues
        }
    }
}

export function doActiveQueue(queuesArrayId) {
    return {
        type: 'DO_ACTIVE_QUEUE',
        payload: {
            queuesArrayId
        }
    }
}


//изменить порядок действий в методе
export function changeOrderActions(actions, queuesArrayId) {
    return {
        type: 'CHANGE_ORDER_ACTIONS',
        payload: {
            data: actions,
            queuesArrayId
        }
    }
}

//добавить действие в очередь
export function addActionToQueue(action) {
    return {
        type: 'ADD_ACTIONTOQUEUE',
        payload: {data: action}
    }
}

//Удалить действие из очереди
export function deleteActionFromQueue(queuesArrayId, action) {
    return {
        type: 'DELETE_ACTIONFROMQUEUE',
        payload: {queuesArrayId, action}
    }
}