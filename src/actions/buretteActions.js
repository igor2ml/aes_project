/**
 * Created by i.chernyakov on 09.02.2018.
 */
import daxios from '../clients/directAxios'
import {toastr} from 'react-redux-toastr';

export function setBuretteEmpty(buretteNumber) {
    return function (dispatch) {
        dispatch({
            type: 'SET_BURETTE_EMPTY',
            payload: {}
        });
        return daxios
            .post('/burette/empty', {
                burette: parseInt(buretteNumber)
            })
            .then((res) => {
                console.log(res);
                if (res.data.status) {
                    toastr.success('Опорожнить', 'Бюретка опрожнена');
                    dispatch({
                        type: 'SET_BURETTE_EMPTY_SUCCESS',
                        payload: {

                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Опорожнить бюретку не удалось');
                    dispatch({
                        type: 'SET_BURETTE_EMPTY_ERROR',
                        payload: {
                            data: err
                        }
                    });
                }

            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Опорожнить бюретку не удалось');
                dispatch({
                    type: 'SET_BURETTE_EMPTY_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setBuretteFill(buretteNumber) {
    return function (dispatch) {
        dispatch({
            type: 'SET_BURETTE_FILL',
            payload: {}
        });
        return daxios
            .post('/burette/fill', {
                burette: parseInt(buretteNumber)
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Заполнить', 'Бюретка заполнена');
                    dispatch({
                        type: 'SET_BURETTE_FILL_SUCCESS',
                        payload: {

                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Заполнить бюретку не удалось');
                    dispatch({
                        type: 'SET_BURETTE_FILL_FAIL',
                        payload: {
                        }
                    });
                }

            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Заполнить бюретку не удалось');
                dispatch({
                    type: 'SET_BURETTE_FILL_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setBuretteRinse(buretteNumber) {
    return function (dispatch) {
        dispatch({
            type: 'SET_BURETTE_RINSE',
            payload: {}
        });
        return daxios
            .post('/burette/rinse', {
                burette: parseInt(buretteNumber)
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Промыть', 'Бюретка промыта');
                    dispatch({
                        type: 'SET_BURETTE_RINSE_SUCCESS',
                        payload: {

                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Промыть бюретку не удалось');
                    dispatch({
                        type: 'SET_BURETTE_RINSE_FAIL',
                        payload: {

                        }
                    });
                }
            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Промыть бюретку не удалось');
                dispatch({
                    type: 'SET_BURETTE_RINSE_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}

export function setBurettePour(buretteNumber) {
    return function (dispatch) {
        dispatch({
            type: 'SET_BURETTE_POUR',
            payload: {}
        });
        return daxios
            .post('/burette/pour', {
                burette: parseInt(buretteNumber)
            })
            .then((res) => {
                console.log(res);
                if (res.data.ok) {
                    toastr.success('Слить заданный объем', 'Объем слит');
                    dispatch({
                        type: 'SET_BURETTE_POUR_SUCCESS',
                        payload: {

                        }
                    });
                } else {
                    toastr.error('Ошибка', 'Слить заданный объем не удалось');
                    dispatch({
                        type: 'SET_BURETTE_POUR_FAIL',
                        payload: {

                        }
                    });
                }
            })
            .catch((err) => {
                console.log(err);
                toastr.error('Ошибка', 'Слить заданный объем не удалось');
                dispatch({
                    type: 'SET_BURETTE_POUR_ERROR',
                    payload: {
                        data: err
                    }
                });
            });
    }
}
