/**
 * Created by i.chernyakov on 28.11.2017.
 */
export function toggleShowKeyboard() {
    return {
        type: 'TOGGLE_SHOW-KEYBOARD'
    }
}
