/**
 * Created by i.chernyakov on 24.10.2017.
 */
import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';


import {Page404} from './components/Page404/Page404';


import StartPage from './components/StartPage/StartPage';

import Language from './components/Language/Language';

import Register from './components/Register/Register';

import Settings from './components/Settings/Settings';
import Users from './components/Settings/Users/Users';
import DateTime from './components/Settings/DateTime/DateTime';

import DevicesPage from './components/Settings/DevicesPage/DevicesPage';
import Autosampler from './components/devices/autosampler/Autosampler/Autosampler';
import AutosamplerConfig from './components/devices/autosampler/AutosamplerConfig/AutosamplerConfig';
import AutosamplerHandControl from './components/devices/autosampler/AutosamplerHandControl/AutosamplerHandControl';
import PhBlock from './components/devices/phBlock/PhBlock/PhBlock';
import PhBlockElectrode from './components/devices/phBlock/PhBlockElectrode/PhBlockElectrode';
import PhBlockParams from './components/devices/phBlock/PhBlockParams/PhBlockParams';
import PhBlockCalibration from './components/devices/phBlock/PhBlockCalibration/PhBlockCalibration';
import PhBlockHandControl from './components/devices/phBlock/PhBlockHandControl/PhBlockHandControl';
import PhotoBlockHandControl from './components/devices/photoblock/PhotoBlockHandControl/PhotoBlockHandControl';
import Burette from './components/devices/burette/Burette/Burette';
import BuretteCalibration from './components/devices/burette/BuretteCalibration/BuretteCalibration';
import BuretteKp from './components/devices/burette/BuretteKp/BuretteKp';
import BuretteHandControl from './components/devices/burette/BuretteHandControl/BuretteHandControl';
import Pump from './components/devices/pump/Pump/Pump';
import PumpCalibration from './components/devices/pump/PumpCalibration/PumpCalibration';
import PumpHandControl from './components/devices/pump/PumpHandControl/PumpHandControl';
import HeatElement from './components/devices/heat_element/HeatElement/HeatElement';
import HeatElementHandControl from './components/devices/heat_element/HeatElementHandControl/HeatElementHandControl';
import MagneticStirrer from './components/devices/magnetic_stirrer/MagneticStirrer/MagneticStirrer';
import MagneticStirrerHandControl from './components/devices/magnetic_stirrer/MagneticStirrerHandControl/MagneticStirrerHandControl';
import Libra from './components/devices/libra/Libra/Libra';
import TermoSensor from './components/devices/termo_sensor/TermoSensor/TermoSensor';
import NewTermoSensor from './components/devices/termo_sensor/NewTermoSensor/NewTermoSensor';
import TermoSensorCalibration from './components/devices/termo_sensor/TermoSensorCalibration/TermoSensorCalibration';

import GLP from './components/additionally/GLP/GLP';
import Service from './components/additionally/Service/Service';

import NewMethod from './components/methods/NewMethod/NewMethod';

import AnalysisPage from './components/AnalysisPage/AnalysisPage';
import AnalysisExample from './components/AnalysisPage/AnalysisExample/AnalysisExample';



export class App extends React.Component {
    render() {

        return <BrowserRouter>
            <Switch>

                <Route exact path='/' component={StartPage}/>
                <Route exact path='/register' component={Register}/>
                <Route exact path='/language' component={Language}/>
                <Route exact path='/settings' component={Settings}/>
                <Route exact path='/users' component={Users}/>
                <Route exact path='/users--new' component={Register}/>
                <Route exact path='/date-time' component={DateTime}/>
                <Route exact path='/device' component={DevicesPage}/>

                <Route exact path='/device--autosampler' component={Autosampler}/>
                <Route exact path='/device--autosampler-config' component={AutosamplerConfig}/>
                <Route exact path='/device--autosampler--hand-control' component={AutosamplerHandControl}/>

                <Route exact path='/device--ph-block' component={PhBlock}/>
                <Route exact path='/device--ph-block--electrode' component={PhBlockElectrode}/>
                <Route exact path='/device--ph-block--params' component={PhBlockParams}/>
                <Route exact path='/device--ph-block--calibration' component={PhBlockCalibration}/>
                <Route exact path='/device--ph-block--hand-control' component={PhBlockHandControl}/>

                <Route exact path='/device--photo-block' component={PhotoBlockHandControl}/>
                {/*<Route exact path='/device--photo-block--hand-control' component={PhotoBlockHandControl}/>*/}

                <Route exact path='/device--burette' component={Burette}/>
                <Route exact path='/device--burette--calibration' component={BuretteCalibration}/>
                <Route exact path='/device--burette--calibration-:id' component={BuretteCalibration}/>
                <Route exact path='/device--burette--kp-:id' component={BuretteKp}/>
                <Route exact path='/device--burette--hand-control-:id' component={BuretteHandControl}/>

                <Route exact path='/device--pump' component={Pump}/>
                <Route exact path='/device--pump--calibration' component={PumpCalibration}/>
                <Route exact path='/device--pump--hand-control' component={PumpHandControl}/>

                <Route exact path='/device--heat-element' component={HeatElement}/>
                <Route exact path='/device--heat-element--hand-control' component={HeatElementHandControl}/>

                <Route exact path='/device--magnetic-stirrer' component={MagneticStirrer}/>
                <Route exact path='/device--magnetic-stirrer--hand-control' component={MagneticStirrerHandControl}/>

                <Route exact path='/device--libra' component={Libra}/>

                <Route exact path='/device--termo-sensor' component={TermoSensor}/>
                <Route exact path='/device--termo-sensor--new' component={NewTermoSensor}/>
                <Route exact path='/device--termo-sensor--calibration' component={TermoSensorCalibration}/>

                <Route exact path='/glp' component={GLP}/>
                <Route exact path='/service' component={Service}/>

                <Route exact path='/methods--new' component={NewMethod}/>

                <Route exact path='/analysis' component={AnalysisPage}/>
                <Route exact path='/analysis__example' component={AnalysisExample}/>

                <Route component={Page404}/>
            </Switch>
        </BrowserRouter>
    }
}


